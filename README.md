# Push Me (Recycler View Challenge)

The whole game based on Recycler view

## Description

New puzzle game with push mechanics. Show the Cube King how you can push the blocks to solve the level. Finnish all levels in a chapter to be able to start the next one. Each chapter is unique with its own unique block, which makes the game more interesting and complicated.

### Bulet features

- simple
- light weight
- easy to understand
- sending level to developers team
- working without internet
- creation custom level
- invesntory system
- big amount of game cells
- guides for beginners

### What used

- MVVM architecture
- Motion layout
- Kotlin
- Kotlin DSL
- Kotlin coroutines
- Data binding
- Room database
- LiveData
- AdMob API
- Google Billing API

### Keywords

pushme, blocks, cube, king, orange, cells, blue 

## Download

<a href='https://play.google.com/store/apps/details?id=app.snappik.pushme&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' width="300" height="120" src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>


## Authors

* **Munatayev Timur**
