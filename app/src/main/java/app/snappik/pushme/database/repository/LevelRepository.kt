package app.snappik.pushme.database.repository

import androidx.annotation.WorkerThread
import app.snappik.pushme.database.dao.LevelDao
import app.snappik.pushme.model.Level
import javax.inject.Inject

class LevelRepository @Inject constructor(private val levelDao: LevelDao) {
    @WorkerThread suspend fun insert(level: Level) = levelDao.insert(level)
    @WorkerThread fun getLevelsByChapterId(chapterId: Long) = levelDao.getLevelsByChapterId(chapterId)
    @WorkerThread fun getLevelById(levelId: Long) = levelDao.getLevelById(levelId)
    @WorkerThread fun updateDoneByLevelId(levelId: Long) = levelDao.updateDoneByLevelId(levelId)
    @WorkerThread suspend fun setLastActive(levelId: Long) = levelDao.setAsLastActive(levelId, System.currentTimeMillis())
    @WorkerThread fun getLastActiveLevel() = levelDao.getLastActive()
    @WorkerThread suspend fun countLevels(chapterId: Long) = levelDao.countLevels(chapterId)
    @WorkerThread suspend fun updatePathByLevelId(path: String, chapterId: Long) = levelDao.updatePathByLevelId(chapterId, path)
    @WorkerThread suspend fun updateSolutionByLevelId(solution: String, chapterId: Long) = levelDao.updateSolutionByLevelId(chapterId, solution)
    @WorkerThread suspend fun delete(level: Level) = levelDao.delete(level)
}