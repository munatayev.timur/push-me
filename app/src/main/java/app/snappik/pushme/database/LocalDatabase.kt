package app.snappik.pushme.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import app.snappik.pushme.database.dao.ChapterDao
import app.snappik.pushme.database.dao.GuidDao
import app.snappik.pushme.database.dao.LevelDao
import app.snappik.pushme.database.dao.UserDao
import app.snappik.pushme.model.Chapter
import app.snappik.pushme.model.Guid
import app.snappik.pushme.model.Level
import app.snappik.pushme.model.User

@Database(entities = [User::class, Chapter::class, Level::class, Guid::class], version = 1, exportSchema = false)
abstract class LocalDatabase : RoomDatabase() {
    abstract val userDao: UserDao
    abstract val levelDao: LevelDao
    abstract val chapterDao: ChapterDao
    abstract val guidDao: GuidDao

    companion object{

        private const val DATABASE_NAME = "pusheme1.db"

        @Volatile private var instance: LocalDatabase? = null

        fun getDatabase(context: Context): LocalDatabase =
            instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it } }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, LocalDatabase::class.java, DATABASE_NAME).build()
    }
}