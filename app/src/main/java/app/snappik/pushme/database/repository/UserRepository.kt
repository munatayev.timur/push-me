package app.snappik.pushme.database.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import app.snappik.pushme.database.dao.UserDao
import app.snappik.pushme.model.User
import javax.inject.Inject

class UserRepository @Inject constructor(private val userDao: UserDao) {
    @WorkerThread fun getUser(): LiveData<User> = userDao.getUser()
    @WorkerThread suspend fun insert(user: User) = userDao.insert(user)
    @WorkerThread suspend fun update(user: User) = userDao.update(user)
}