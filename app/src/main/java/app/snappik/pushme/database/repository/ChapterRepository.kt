package app.snappik.pushme.database.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import app.snappik.pushme.database.dao.ChapterDao
import app.snappik.pushme.model.Chapter
import javax.inject.Inject

class ChapterRepository @Inject constructor(private val chapterDao: ChapterDao) {
    @WorkerThread fun getChapters(): LiveData<List<Chapter>> = chapterDao.getChapters()
    @WorkerThread suspend fun insertChapter(chapter: Chapter) = chapterDao.insert(chapter)
    @WorkerThread suspend fun buyChapter(chapterId: Long) = chapterDao.buyChapter(chapterId)
}