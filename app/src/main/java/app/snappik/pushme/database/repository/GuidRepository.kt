package app.snappik.pushme.database.repository

import androidx.annotation.WorkerThread
import app.snappik.pushme.database.dao.GuidDao
import app.snappik.pushme.model.Guid
import javax.inject.Inject

class GuidRepository @Inject constructor(private val guidDao: GuidDao) {
    @WorkerThread fun getGuidByLevel(levelId: Int, chapterId: Int) = guidDao.getGuid(levelId, chapterId)
    @WorkerThread suspend fun insert(guid: Guid) = guidDao.insert(guid)
}