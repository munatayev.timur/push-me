package app.snappik.pushme.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.snappik.pushme.model.Guid

@Dao
interface GuidDao {
    @Query("SELECT * FROM guid WHERE level = :levelId AND chapterId =:mChapterId")
    fun getGuid(levelId: Int, mChapterId: Int): LiveData<Guid>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(guid: Guid)
}