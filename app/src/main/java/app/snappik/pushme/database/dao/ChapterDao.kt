package app.snappik.pushme.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.snappik.pushme.model.Chapter

@Dao
interface ChapterDao {
    @Query("SELECT * FROM chapter")
    fun getChapters(): LiveData<List<Chapter>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(chapter: Chapter)

    @Query("UPDATE chapter SET isBought = 1 WHERE id =:chapterId")
    suspend fun buyChapter(chapterId: Long)
}