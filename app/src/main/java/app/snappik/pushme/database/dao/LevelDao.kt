package app.snappik.pushme.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import app.snappik.pushme.model.Level

@Dao
interface LevelDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(level: Level)

    @Delete
    suspend fun delete(level: Level)

    @Query("SELECT * FROM level WHERE chapterId = :chapterId")
    fun getLevelsByChapterId(chapterId: Long): LiveData<List<Level>>

    @Query("SELECT * FROM level WHERE id = :levelId ORDER BY id DESC LIMIT 1")
    fun getLevelById(levelId: Long): LiveData<Level>

    @Query("UPDATE level SET isDone = 1 WHERE id =:levelId")
    fun updateDoneByLevelId(levelId: Long)

    @Query("UPDATE level SET lastEntering = :time WHERE id =:levelId")
    suspend fun setAsLastActive(levelId: Long, time: Long)

    @Query("SELECT * FROM level ORDER BY lastEntering DESC LIMIT 1")
    fun getLastActive(): LiveData<Level>

    @Query("SELECT COUNT(id) FROM level WHERE chapterId=:chapterId")
    suspend fun countLevels(chapterId: Long): Int

    @Query("UPDATE level SET path = :path WHERE id =:levelId")
    suspend fun updatePathByLevelId(levelId: Long, path: String)

    @Query("UPDATE level SET solution = :solution WHERE id =:levelId")
    suspend fun updateSolutionByLevelId(levelId: Long, solution: String)
}