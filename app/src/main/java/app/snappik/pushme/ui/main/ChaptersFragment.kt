package app.snappik.pushme.ui.main

import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import app.snappik.pushme.BR
import app.snappik.pushme.R
import app.snappik.pushme.databinding.FragmentChaptersBinding
import app.snappik.pushme.model.Chapter
import app.snappik.pushme.observer.IChapterObserver
import app.snappik.pushme.observer.ILevelObserver
import app.snappik.pushme.observer.IUserObserver
import app.snappik.pushme.ui.base.AbstractFragment
import app.snappik.pushme.viewmodel.ChapterViewModel
import app.snappik.pushme.viewmodel.LevelViewModel
import app.snappik.pushme.viewmodel.UserViewModel
import app.snappik.pushme.viewmodel.transformations.getChapterLevels
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ChaptersFragment : AbstractFragment<FragmentChaptersBinding>(), IChapterObserver, IUserObserver, ILevelObserver{

    @Inject override lateinit var chapterViewModel: ChapterViewModel

    @Inject override lateinit var userViewModel: UserViewModel

    @Inject override lateinit var levelViewModel: LevelViewModel

    override val viewId: Int = R.layout.fragment_chapters

    override fun FragmentChaptersBinding.initVariables() {
        fragment = this@ChaptersFragment
        layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToChapters()
    }

    override fun onChaptersFetched(list: List<Chapter>) {
        BR.chapterAdapter.updateWith {
            chapterAdapter?.onChapterBought(list) ?: initAdapter(list)
        }
    }

    private fun FragmentChaptersBinding.initAdapter(list: List<Chapter>){
        chapterAdapter = ChaptersAdapter(
            arguments?.getLong(HIGHLIGHT_CHAPTER_ID),
            this@ChaptersFragment,
            list,
            levelInfo = { id -> calculateLevels(id) },
            onChapterBuy = { cost, id -> onBuyChapter(id, cost) },
            onChapterSelected = { id -> onOpenChapter(id) }
        )
    }

    private fun calculateLevels(chapterId: Long): LiveData<String>?{
        return levelViewModel.getLevelsByChapterId(chapterId)?.getChapterLevels()
    }

    private fun onBuyChapter(chapterId: Long, cost: Int){
        if(userViewModel.user?.value?.coins ?: 0 >= cost){
            chapterViewModel.buyChapter(chapterId)
            userViewModel.updateCoins(-cost)
        } else openCoins(childFragmentManager)
    }

    private fun onOpenChapter(chapterId: Long){
        findNavController().navigate(ChaptersFragmentDirections.actionChaptersFragmentToLevelsFragment(chapterId))
    }

    companion object {
        const val HIGHLIGHT_CHAPTER_ID  = "highlight"
    }
}