package app.snappik.pushme.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

abstract class AbstractDialogFragment<T : ViewDataBinding>(
    private val viewId: Int,
    private val styleId: Int
) :
    DialogFragment(),
    IDataBinding<T> {

    override lateinit var viewDataBinding: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, viewId, container, false)
        viewDataBinding.initVariables()
        return viewDataBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, styleId)
    }

    abstract fun T.initVariables()

    open fun show(manager: FragmentManager) {
        if (manager.isStateSaved.not()) {
            super.show(manager, tag ?: this::class.java.name)
        }
    }
}