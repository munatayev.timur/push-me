package app.snappik.pushme.ui.dialog

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import app.snappik.pushme.BR
import app.snappik.pushme.R
import app.snappik.pushme.databinding.DialogCoinsBinding
import app.snappik.pushme.model.BillingProduct
import app.snappik.pushme.model.User
import app.snappik.pushme.observer.IBillingObserver
import app.snappik.pushme.observer.IUserObserver
import app.snappik.pushme.ui.base.AbstractDialogFragment
import app.snappik.pushme.utils.BillingManager
import app.snappik.pushme.viewmodel.UserViewModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class CoinsDialogFragment : AbstractDialogFragment<DialogCoinsBinding>(
        R.layout.dialog_coins,
        R.style.TransparentDialog
), IBillingObserver, IUserObserver {

    @Inject override lateinit var userViewModel: UserViewModel

    @Inject override lateinit var billingManager: BillingManager

    private val onPurchaseSuccess = fun(amount: Int) {
        activity?.supportFragmentManager?.let {
            LevelDialog(null, amount) {
                userViewModel.updateCoins(amount)
                if(viewDataBinding.bonusApplied == true){
                    billingManager.establishConnection(false)
                    userViewModel.madePurchase()
                }
            }.show(it)
        }
        dismiss()
    }

    private val adLoadCallback = object : RewardedAdLoadCallback() {
        override fun onAdLoaded(rewardedAd: RewardedAd) {
            showRewardedAdd(rewardedAd)
            BR.adLoading.updateWith { adLoading = false }
        }

        override fun onAdFailedToLoad(error: LoadAdError) {
            ConfirmDialog(error.message, getString(R.string.ok)).show(childFragmentManager)
            BR.adLoading.updateWith { adLoading = false }
        }
    }

    override fun onUserFetched(user: User) {
        BR.bonusApplied.updateWith{ bonusApplied = user.isFirstBuy }
    }

    override fun DialogCoinsBinding.initVariables() {
        dialog = this@CoinsDialogFragment
        layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        billingManager.success = onPurchaseSuccess
        fetchProducts()
        subscribeToUserUpdate()
    }

    override fun onProductsFetched(list: List<BillingProduct>) {
        BR.billingAdapter.updateWith {
            billingAdapter = BillingAdapter(this@CoinsDialogFragment, list) {
                billingManager.purchase(activity, it.original)
            }
        }
    }

    fun showAdd() {
        BR.adLoading.updateWith { adLoading = true }
        val request = AdRequest.Builder().build()
        RewardedAd.load(requireContext(), COINS_100_AD, request, adLoadCallback)
    }

    private fun showRewardedAdd(rewardedAd: RewardedAd) {
        rewardedAd.show(requireActivity()) {
            onPurchaseSuccess(it.type.toInt())
        }
    }
    companion object {
        private const val COINS_100_AD = "ca-app-pub-5216346427667829/5123006926"
    }
}