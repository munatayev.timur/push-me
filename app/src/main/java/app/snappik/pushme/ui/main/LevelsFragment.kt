package app.snappik.pushme.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import app.snappik.pushme.BR
import app.snappik.pushme.R
import app.snappik.pushme.databinding.FragmentLevelsBinding
import app.snappik.pushme.model.Level
import app.snappik.pushme.observer.ILevelObserver
import app.snappik.pushme.observer.IUserObserver
import app.snappik.pushme.ui.base.AbstractFragment
import app.snappik.pushme.ui.scene.SceneActivity
import app.snappik.pushme.ui.scene.SceneFragment.Companion.LEVEL_ID
import app.snappik.pushme.viewmodel.LevelViewModel
import app.snappik.pushme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LevelsFragment : AbstractFragment<FragmentLevelsBinding>(), ILevelObserver, IUserObserver {

    @Inject override lateinit var levelViewModel: LevelViewModel

    @Inject override lateinit var userViewModel: UserViewModel

    override val viewId: Int = R.layout.fragment_levels

    override fun FragmentLevelsBinding.initVariables() {
        fragment = this@LevelsFragment
        layoutManager = GridLayoutManager(requireContext(), 5)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getLong(CHAPTER_ID, -1)?.let {
            BR.isCustom.updateWith { isCustom = it < 0 }
            fetchLevels(it)
        }
    }

    override fun onLevelsFetched(list: List<Level>) {
        BR.levelsAdapter.updateWith{
            levelsAdapter = LevelsAdapter(this@LevelsFragment, list){
                startActivity(Intent(requireContext(), SceneActivity::class.java).apply {
                    putExtra(LEVEL_ID, it.id)
                })
            }
        }
    }

    fun onAddLevel() = levelViewModel.addLevel()

    companion object {
        const val CHAPTER_ID  = "chapterId"
    }
}