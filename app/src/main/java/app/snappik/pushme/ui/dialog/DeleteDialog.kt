package app.snappik.pushme.ui.dialog

import app.snappik.pushme.R
import app.snappik.pushme.databinding.DialogDeleteBinding
import app.snappik.pushme.model.Level
import app.snappik.pushme.ui.base.AbstractDialogFragment
import app.snappik.pushme.viewmodel.LevelViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DeleteDialog(
    private val level: Level,
    private val onDeleted: () -> Unit
) : AbstractDialogFragment<DialogDeleteBinding>(
    R.layout.dialog_delete,
    R.style.TransparentDialog
) {

    @Inject lateinit var levelViewModel: LevelViewModel

    override fun DialogDeleteBinding.initVariables() {
        dialog = this@DeleteDialog
    }

    fun confirmDeletion(){
        levelViewModel.deleteLevel(level)
        dismiss()
        onDeleted.invoke()
    }
}