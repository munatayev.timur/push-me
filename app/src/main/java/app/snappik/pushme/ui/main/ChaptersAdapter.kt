 package app.snappik.pushme.ui.main

import android.graphics.Color.parseColor
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import app.snappik.pushme.R
import app.snappik.pushme.databinding.ItemChapterBinding
import app.snappik.pushme.model.Chapter


 class ChaptersAdapter(
        private val highlightId: Long?,
        private val lifecycleOwner: LifecycleOwner,
        unwrappedList: List<Chapter>,
        private val levelInfo: (chapterId: Long) -> LiveData<String>?,
        private val onChapterSelected: (chapterId: Long) -> Unit,
        private val onChapterBuy: (cost: Int, chapterId: Long) -> Unit
) : RecyclerView.Adapter<ChaptersAdapter.ChapterViewHolder>() {

     private val list = ArrayList<Wrapped>()

     init { list.addAll(unwrappedList.map { Wrapped(highlightId == it.id, it) }) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChapterViewHolder {
        val view = DataBindingUtil.inflate<ItemChapterBinding>(LayoutInflater.from(parent.context), R.layout.item_chapter, parent, false)
        view.lifecycleOwner = lifecycleOwner
        return ChapterViewHolder(view, list)
    }

     fun onChapterBought(newList: List<Chapter>) = list.forEachIndexed { index, pair ->
         newList.find { it.id == pair.chapter.id && it.isBought != pair.chapter.isBought }?.let {
             list[index] = Wrapped(true, it)
             notifyItemChanged(index)
         }
     }

    override fun onBindViewHolder(holder: ChapterViewHolder, position: Int) = holder.bind(list[position].chapter)

    fun ChapterViewHolder.bind(mChapter: Chapter) = with(binding){
        chapter = mChapter
        levelInfo = levelInfo(mChapter.id)
        setOnPlay { onChapterSelected.invoke(mChapter.id) }
        setOnBuy {  onChapterBuy.invoke(mChapter.cost, mChapter.id) }
        viewHolder = this@bind
        val colors = intArrayOf(parseColor(mChapter.topColorHex), parseColor(mChapter.bottomColorHex))
        bachground = GradientDrawable(TOP_BOTTOM, colors)
        invalidateAll()
    }

    override fun getItemCount(): Int = list.count()

    class ChapterViewHolder(val binding: ItemChapterBinding, val list:ArrayList<Wrapped>): RecyclerView.ViewHolder(binding.root){
        fun isExpanded() = list[adapterPosition].isExpanded
        fun changeExpanded(){
            list[adapterPosition].isExpanded = list[adapterPosition].isExpanded.not()
            binding.invalidateAll()
        }
    }

     class Wrapped(var isExpanded: Boolean, var chapter: Chapter)
}