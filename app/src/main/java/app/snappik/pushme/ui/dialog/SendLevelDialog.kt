package app.snappik.pushme.ui.dialog

import android.widget.EditText
import app.snappik.pushme.R
import app.snappik.pushme.databinding.DialogSendBinding
import app.snappik.pushme.ui.base.AbstractDialogFragment
import app.snappik.pushme.utils.EmailSender
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SendLevelDialog(
    private val path: String,
    private val solution: String
)  : AbstractDialogFragment<DialogSendBinding>(
        R.layout.dialog_send,
        R.style.TransparentDialog
) {

    @Inject
    lateinit var emailSender: EmailSender

    override fun DialogSendBinding.initVariables() {
        dialog = this@SendLevelDialog
    }

    fun send(authorField: EditText){
        emailSender.send(authorField.text.toString(), path, solution)
        dismiss()
    }
}