package app.snappik.pushme.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.snappik.pushme.R

abstract class AbstractActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_abstract_fullscreen)
        disableAnimationBetweenActivities()
    }

    private fun disableAnimationBetweenActivities() = with(window){
        allowEnterTransitionOverlap = false
        allowReturnTransitionOverlap = false
        exitTransition = null
        enterTransition = null
        overridePendingTransition(0, 0)
    }

    fun changeFragment(fragmentClass: Class<out AbstractFragment<*>>, bundle: Bundle? = null) {
        supportFragmentManager.fragmentFactory.instantiate(classLoader, fragmentClass.name).also {
            supportFragmentManager.beginTransaction().replace(R.id.container, fragmentClass, bundle, fragmentClass.name).commit()
        }
    }
}