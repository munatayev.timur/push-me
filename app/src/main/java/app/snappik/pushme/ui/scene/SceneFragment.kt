package app.snappik.pushme.ui.scene

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.DragEvent
import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.snappik.pushme.BR
import app.snappik.pushme.R
import app.snappik.pushme.databinding.FragmentSceneBinding
import app.snappik.pushme.engine.EngineAdapter
import app.snappik.pushme.engine.IActionEvent
import app.snappik.pushme.engine.SolutionAdapter
import app.snappik.pushme.engine.StoreAdapter
import app.snappik.pushme.model.Cell
import app.snappik.pushme.model.Guid
import app.snappik.pushme.model.Level
import app.snappik.pushme.model.User
import app.snappik.pushme.observer.IGuidObserver
import app.snappik.pushme.observer.ILevelObserver
import app.snappik.pushme.observer.IUserObserver
import app.snappik.pushme.ui.base.AbstractFragment
import app.snappik.pushme.ui.dialog.*
import app.snappik.pushme.utils.ParseUtils.parsePath
import app.snappik.pushme.utils.ParseUtils.parseSolution
import app.snappik.pushme.utils.ParseUtils.parseStore
import app.snappik.pushme.utils.ParseUtils.parseToPathString
import app.snappik.pushme.utils.ParseUtils.parseToSolutionString
import app.snappik.pushme.utils.smoothSnapToPosition
import app.snappik.pushme.view.IViewHolder
import app.snappik.pushme.view.guid.GuidLayout.Companion.WAIT_BEFORE_GUID
import app.snappik.pushme.viewmodel.GuidViewModel
import app.snappik.pushme.viewmodel.LevelViewModel
import app.snappik.pushme.viewmodel.UserViewModel
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SceneFragment : AbstractFragment<FragmentSceneBinding>(),
        IActionEvent,
        ILevelObserver,
        IUserObserver,
        IGuidObserver {

    override val viewId: Int = R.layout.fragment_scene

    @Inject
    override lateinit var guidViewModel: GuidViewModel

    @Inject
    override lateinit var userViewModel: UserViewModel

    @Inject
    override lateinit var levelViewModel: LevelViewModel

    override fun FragmentSceneBinding.initVariables() {
        fragment = this@SceneFragment
        layoutManagerMain = GridLayoutManager(requireContext(), 7)
        layoutManagerBottom = GridLayoutManager(requireContext(), 1, GridLayoutManager.HORIZONTAL, false)
        layoutManagerInventory = GridLayoutManager(requireContext(), 1, GridLayoutManager.HORIZONTAL, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToAdds()
        arguments?.getLong(LEVEL_ID)?.let {
            fetchLevel(it)
        } ?: fetchLastActive()
    }

    override fun onLevelFetched(level: Level) {
        levelViewModel.setAsLastActive(level.id)
        if (level.chapterId > 1) subscribeToUserUpdate()

        BR.level.updateWith {
            this.level = level
        }

        val path = level.path.parsePath()
        BR.adapterMain.updateWith {
            adapterMain = EngineAdapter(path.toMutableList(), this@SceneFragment)
        }
        val solution = level.solution.parseSolution()
        BR.adapterBottom.updateWith {
            adapterBottom = SolutionAdapter(solution, this@SceneFragment)
        }

        getGuidForLevel(level.index, level.chapterId)

        //guid to show inventory
        if (level.isDone.not() && level.chapterId == 2 && level.index == 6 && level.lastEntering == 0L) {
            userViewModel.addToInventory("F")
        }
    }

    override fun onUserFetched(user: User) {
        val inventory = user.inventory.parseStore()
        if (inventory.isEmpty()) return
        BR.adapterBottom.updateWith {
            adapterInventory = StoreAdapter(ArrayList(inventory), this@SceneFragment)
        }
    }

    override fun dragFinished(from: Int, to: Int, adapterId: Int) {
        when (adapterId) {
            BR.adapterBottom -> adapterId.updateWith {
                adapterBottom?.getByPositionAndRemove(from)?.let { cell ->
                    BR.adapterMain.updateWith { adapterMain?.replace(to, cell) }
                }
            }
            BR.adapterInventory -> adapterId.updateWith {
                adapterInventory?.getByPositionAndRemove(from)?.let { cell ->
                    BR.adapterMain.updateWith { adapterMain?.replace(to, cell) }
                }
            }
        }
    }

    override fun showDoneDialog(level: Level) {
        if (isDetached) return
        if (level.isDone.not()) {
            userViewModel.updateCoins(level.coins)
            levelViewModel.updateDoneByLevelId(level.id)
        }
        LevelDialog(if (level.isLast) null else false, if (level.isDone) 0 else level.coins) {
            when {
                viewDataBinding.level?.chapterId ?: 1 < 0 -> refreshLevel()
                level.isLast -> getParentActivity<SceneActivity>()?.finish()
                else -> {
                    val nextLevel = level.id + 1L
                    fetchLevel(nextLevel)
                    arguments?.putLong(LEVEL_ID, nextLevel)
                }
            }
        }.show(childFragmentManager)
    }

    override fun allFilled() {
        levelViewModel.updateCounter()
        arguments?.getLong(LEVEL_ID)?.let { fetchLevel(it, true) } ?: fetchLastActive(true)
        BR.adapterInventory.updateWith {
            adapterInventory?.getAllInventoryToSave()?.let { inventoryToSave ->
                userViewModel.saveInventory(inventoryToSave)
            }
        }
    }

    override fun gameOver() {
        levelViewModel.updateCounter()
        LevelDialog(true) { refreshLevel() }.show(childFragmentManager)
    }

    override fun isSolutionContainsFilled(): Boolean {
        return viewDataBinding.adapterBottom?.isContains(Cell.Filled::class) ?: false ||
                viewDataBinding.adapterInventory?.isContains(Cell.Filled::class) ?: false
    }

    fun refreshLevel() = arguments?.getLong(LEVEL_ID)?.let { fetchLevel(it) } ?: fetchLastActive()

    fun saveLevel(levelId: Long, mainAdapter: EngineAdapter, bottomAdapter: SolutionAdapter) {
        val path = mainAdapter.getExistingList().parseToPathString()
        val solution = bottomAdapter.getExistingList().parseToSolutionString()
        levelViewModel.setSolution(levelId, solution)
        levelViewModel.setPath(levelId, path)
        ConfirmDialog(getString(R.string.level_saved), getString(R.string.ok)).show(childFragmentManager)
    }

    fun deleteLevel(level: Level) = DeleteDialog(level) {
        getParentActivity<SceneActivity>()?.finish()
    }.show(childFragmentManager)

    fun sendLevel(mainAdapter: EngineAdapter, bottomAdapter: SolutionAdapter) {
        val path = mainAdapter.getExistingList().parseToPathString()
        val solution = bottomAdapter.getExistingList().parseToSolutionString()
        SendLevelDialog(path, solution).show(childFragmentManager)
    }

    override fun onClicked(cell: Cell, position: Int, adapterId: Int) {
        if (viewDataBinding.level?.chapterId ?: 1 < 0) {
            CellsDialogFragment(false) {
                BR.adapterMain.updateWith { adapterMain?.replace(position, it, checkCells = false, isFromDialogs = true) }
            }.show(childFragmentManager)
        }
    }

    fun addSolutionCell() {
        CellsDialogFragment(true) {
            BR.adapterBottom.updateWith { adapterBottom?.addElement(it as Cell.Solution) }
        }.show(childFragmentManager)
    }

    fun getRemoveSolution() = View.OnDragListener { view, event ->
        when (event.action) {
            DragEvent.ACTION_DRAG_STARTED -> (view as? Button)?.text = getString(R.string.custom_bin)
            DragEvent.ACTION_DRAG_ENTERED -> {
                view.scaleY = 1.2F
                view.scaleX = 1.2F
            }
            DragEvent.ACTION_DRAG_EXITED -> {
                view.scaleY = 1.0F
                view.scaleX = 1.0F
            }
            DragEvent.ACTION_DROP -> {
                event.clipData.getItemAt(0).intent.extras?.let { extras ->
                    val position = extras.getInt(IViewHolder.POSITION)
                    BR.adapterBottom.updateWith {
                        adapterBottom?.getByPositionAndRemove(position)
                    }
                }
            }
            DragEvent.ACTION_DRAG_ENDED -> {
                (view as? Button)?.text = getString(R.string.custom_add_cell)
                view.scaleY = 1.0F
                view.scaleX = 1.0F
                val sView = event.localState as? View
                sView?.alpha = 1F
            }
            else -> Unit
        }
        true
    }

    private val addCallback = object : InterstitialAdLoadCallback() {
        override fun onAdFailedToLoad(adError: LoadAdError) {
            levelViewModel.updateCounter()
        }

        override fun onAdLoaded(interstitialAd: InterstitialAd) {
            interstitialAd.fullScreenContentCallback = object : FullScreenContentCallback() {
                override fun onAdDismissedFullScreenContent() {
                    levelViewModel.updateCounter()
                }

                override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
                    levelViewModel.updateCounter()
                }

                override fun onAdShowedFullScreenContent() {
                    levelViewModel.updateCounter()
                }
            }
            activity?.let { interstitialAd.show(it) }
        }
    }

    override fun onShowAdd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(requireContext(), BETWEEN_LEVELS_AD, adRequest, addCallback)
    }

    override fun onGuidFetched(guid: Guid) {
        if (guid.level.toInt() != viewDataBinding.level?.index) return
        BR.containsGuid.updateWith { containsGuid = true }
        Handler(Looper.getMainLooper()).postDelayed({

            var startViewHolder: RecyclerView.ViewHolder? = null
            var endViewHolder: RecyclerView.ViewHolder? = null

            fun checkGuid() {
                startViewHolder?.let { start ->
                    endViewHolder?.let { end ->
                        viewDataBinding.root.findViewById<app.snappik.pushme.view.guid.GuidLayout>(R.id.guid).apply {
                            showDrag(start, end, guid)
                        }
                    }
                }
            }

            val adapterId = if (guid.adapter == 0) R.id.solution_rv else R.id.inventory_rv

            viewDataBinding.root.findViewById<RecyclerView>(R.id.main_rv)?.post {
                endViewHolder = viewDataBinding.root.findViewById<RecyclerView>(R.id.main_rv).findViewHolderForAdapterPosition(guid.endView)
                checkGuid()
            }
            viewDataBinding.root.findViewById<RecyclerView>(adapterId)?.post {
                startViewHolder = viewDataBinding.root.findViewById<RecyclerView>(adapterId).findViewHolderForAdapterPosition(guid.startView)
                checkGuid()
            }
        }, WAIT_BEFORE_GUID)
    }

    override fun noGuid() {
        viewDataBinding.adapterBottom?.let {
            val rv = viewDataBinding.root.findViewById<RecyclerView>(R.id.solution_rv)
            rv.smoothSnapToPosition(rv.adapter?.itemCount ?: 0)
        }
    }

    companion object {
        const val LEVEL_ID = "LEVEL_ID"
        private const val BETWEEN_LEVELS_AD = "ca-app-pub-5216346427667829/4534637967"
    }
}