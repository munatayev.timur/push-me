package app.snappik.pushme.ui.scene

import android.os.Bundle
import app.snappik.pushme.ui.base.AbstractActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SceneActivity : AbstractActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeFragment(SceneFragment::class.java, intent.extras)
    }
}