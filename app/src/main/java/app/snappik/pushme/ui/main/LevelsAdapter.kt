package app.snappik.pushme.ui.main


import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import app.snappik.pushme.R
import app.snappik.pushme.databinding.ItemLevelBinding
import app.snappik.pushme.model.Level

class LevelsAdapter(
        private val lifecycleOwner: LifecycleOwner,
        private val list: List<Level>,
        private val onLevelClicked:(level: Level) -> Unit
) : RecyclerView.Adapter<LevelsAdapter.LevelViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LevelViewHolder {
        val view = DataBindingUtil.inflate<ItemLevelBinding>(LayoutInflater.from(parent.context), R.layout.item_level, parent, false)
            .apply {
                root.layoutParams = ConstraintLayout.LayoutParams(parent.width/5, parent.width/5)
                root.findViewById<View>(R.id.card_star).layoutParams.height = parent.width/17
                lifecycleOwner = this@LevelsAdapter.lifecycleOwner
            }
        return LevelViewHolder(view)
    }

    override fun onBindViewHolder(holder: LevelViewHolder, position: Int) = holder.bind(list[position], position)

    override fun getItemCount(): Int = list.count()

    fun LevelViewHolder.bind(level: Level, position: Int) = with(binding){
        setOnClick { onLevelClicked.invoke(level) }
        levelIndex = "${position + 1}"
        levelColor = Color.BLUE
        isDone = level.isDone
        isNotCustom = level.chapterId > 0
        invalidateAll()
    }

    class LevelViewHolder(val binding: ItemLevelBinding): RecyclerView.ViewHolder(binding.root)
}