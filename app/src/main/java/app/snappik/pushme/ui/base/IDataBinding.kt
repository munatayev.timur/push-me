package app.snappik.pushme.ui.base

import androidx.databinding.ViewDataBinding

interface IDataBinding<T : ViewDataBinding> {
    var viewDataBinding: T
    fun Int.updateWith(change: T.() -> Unit){
        change.invoke(viewDataBinding)
        viewDataBinding.notifyPropertyChanged(this)
    }
}