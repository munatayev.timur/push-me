package app.snappik.pushme.ui.dialog

import app.snappik.pushme.R
import app.snappik.pushme.databinding.DialogConfirmBinding
import app.snappik.pushme.ui.base.AbstractDialogFragment

class ConfirmDialog(
        private val text: String,
        private val button: String
) : AbstractDialogFragment<DialogConfirmBinding>(
        R.layout.dialog_confirm,
        R.style.TransparentDialog
) {
    override fun DialogConfirmBinding.initVariables() {
        dialog = this@ConfirmDialog
        confirmText = text
        confirmButtonText = button
    }
}