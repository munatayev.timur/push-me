package app.snappik.pushme.ui.dialog

import app.snappik.pushme.R
import app.snappik.pushme.databinding.DialogLevelBinding
import app.snappik.pushme.ui.base.AbstractDialogFragment

class LevelDialog(private val isRestart: Boolean?,
                  private val coins: Int = 0,
                  private val onOkClicked:() -> Unit) : AbstractDialogFragment<DialogLevelBinding>(
        R.layout.dialog_level,
        R.style.TransparentDialog
) {

    init {
        isCancelable = false
    }

    override fun DialogLevelBinding.initVariables() {
        dialog = this@LevelDialog
        isRestart = this@LevelDialog.isRestart
        levelDialogCoins = this@LevelDialog.coins
    }

    fun onClick(){
        onOkClicked.invoke()
        dismiss()
    }
}