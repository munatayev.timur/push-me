package app.snappik.pushme.ui.main

import android.os.Bundle
import app.snappik.pushme.R
import app.snappik.pushme.ui.base.AbstractActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AbstractActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}