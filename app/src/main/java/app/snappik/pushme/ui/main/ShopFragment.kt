package app.snappik.pushme.ui.main

import androidx.constraintlayout.motion.widget.MotionLayout
import app.snappik.pushme.BR
import app.snappik.pushme.R
import app.snappik.pushme.databinding.FragmentShopBinding
import app.snappik.pushme.observer.IUserObserver
import app.snappik.pushme.ui.base.AbstractFragment
import app.snappik.pushme.utils.ParseUtils.parseToStringString
import app.snappik.pushme.utils.get3RandomItems
import app.snappik.pushme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ShopFragment : AbstractFragment<FragmentShopBinding>(), IUserObserver {

    override val viewId: Int = R.layout.fragment_shop

    @Inject override lateinit var userViewModel: UserViewModel

    override fun FragmentShopBinding.initVariables() {
        fragment = this@ShopFragment
        transitionState = R.id.chest_start
    }

    fun onBuy(coins: Int, motionLayout: MotionLayout){
        viewDataBinding.apply {
            val isBuy = transitionState == R.id.chest_start
            if(isBuy){
                if (coins < CHEST_COST) {
                    notEnoughMoney()
                    return
                }

                val items = get3RandomItems()
                BR.firstItem.updateWith { firstItem = items[0] }
                BR.secondItem.updateWith { secondItem = items[1] }
                BR.thirdItem.updateWith { thirdItem = items[2] }

                val itemsToSave = items.mapNotNull { it.second }.parseToStringString()
                val moneyToAdd = items.filter { it.second == null }.map { it.first }.sum()
                if(itemsToSave.isNotEmpty()) userViewModel.addToInventory(itemsToSave)
                userViewModel.updateCoins(-CHEST_COST + moneyToAdd)
            }

            BR.transitionState.updateWith{
                transitionState = if(isBuy) R.id.chest_end else R.id.chest_start
                motionLayout.setTransitionDuration(if(isBuy) 3000 else 1)
            }
        }
    }

    private fun notEnoughMoney(){
        openCoins(childFragmentManager)
    }

    companion object {
        private const val CHEST_COST = 100
    }
}