package app.snappik.pushme.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import app.snappik.pushme.R
import app.snappik.pushme.databinding.FragmentMainBinding
import app.snappik.pushme.model.Level
import app.snappik.pushme.model.User
import app.snappik.pushme.observer.ILevelObserver
import app.snappik.pushme.observer.IUserObserver
import app.snappik.pushme.ui.base.AbstractFragment
import app.snappik.pushme.ui.scene.SceneActivity
import app.snappik.pushme.utils.BillingManager
import app.snappik.pushme.viewmodel.LevelViewModel
import app.snappik.pushme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : AbstractFragment<FragmentMainBinding>(), ILevelObserver, IUserObserver {

    override val viewId: Int = R.layout.fragment_main

    @Inject override lateinit var levelViewModel: LevelViewModel

    @Inject override lateinit var userViewModel: UserViewModel

    @Inject lateinit var billingManager: BillingManager

    override fun FragmentMainBinding.initVariables() {
        fragment = this@MainFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchUser()
    }

    fun onPlayClicked() {
        fetchLastActive()
    }

    fun onChaptersClicked() = findNavController().navigate(MainFragmentDirections.actionMainFragmentToChaptersFragment(-1))

    fun onShopClicked() = findNavController().navigate(MainFragmentDirections.actionMainFragmentToShopFragment())

    fun onCustomClicked() = findNavController().navigate(MainFragmentDirections.actionMainFragmentToLevelsFragment(-1))

    override fun onUserFetched(user: User) = billingManager.establishConnection(user.isFirstBuy)

    override fun onLevelFetched(level: Level) {
        if(level.isDone && level.isLast) {
            findNavController().navigate(MainFragmentDirections.actionMainFragmentToChaptersFragment(level.chapterId + 1L))
        } else startActivity(Intent(requireContext(), SceneActivity::class.java))
    }
}