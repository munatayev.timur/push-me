package app.snappik.pushme.ui.dialog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import app.snappik.pushme.R
import app.snappik.pushme.databinding.ItemBillingProductBinding
import app.snappik.pushme.model.BillingProduct

class BillingAdapter(
    private val lifecycleOwner: LifecycleOwner,
    private val list: List<BillingProduct>,
    private val onLevelClicked:(product: BillingProduct) -> Unit
) : RecyclerView.Adapter<BillingAdapter.BillingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BillingViewHolder {
        val view = DataBindingUtil.inflate<ItemBillingProductBinding>(LayoutInflater.from(parent.context), R.layout.item_billing_product, parent, false)
            .apply { lifecycleOwner = this@BillingAdapter.lifecycleOwner }
        return BillingViewHolder(view)
    }

    override fun onBindViewHolder(holder: BillingViewHolder, position: Int) = holder.bind(list[position])

    override fun getItemCount(): Int = list.count()

    fun BillingViewHolder.bind(product: BillingProduct) = with(binding){
        price = product.price
        title = product.title
        onClick = Runnable { onLevelClicked.invoke(product) }
        invalidateAll()
    }

    class BillingViewHolder(val binding: ItemBillingProductBinding): RecyclerView.ViewHolder(binding.root)
}