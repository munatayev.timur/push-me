package app.snappik.pushme.ui.splash

import android.os.Bundle
import app.snappik.pushme.ui.base.AbstractActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : AbstractActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeFragment(SplashFragment::class.java)
    }
}