package app.snappik.pushme.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class AbstractFragment<T : ViewDataBinding> : Fragment(), IDataBinding<T> {

    abstract val viewId: Int

    override lateinit var viewDataBinding: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(layoutInflater, viewId, container, false)
        viewDataBinding.initVariables()
        viewDataBinding.lifecycleOwner = this
        return viewDataBinding.root
    }

    abstract fun T.initVariables()

    protected inline fun <reified T> getParentActivity(): T? = if(activity is T) (activity as T) else null

    fun getStringByRes(res: Int) = if(res != 0)  super.getString(res) else null
}