package app.snappik.pushme.ui.dialog

import android.widget.SeekBar
import androidx.recyclerview.widget.GridLayoutManager
import app.snappik.pushme.BR
import app.snappik.pushme.R
import app.snappik.pushme.databinding.DialogCellsBinding
import app.snappik.pushme.engine.AbstractCellAdapter
import app.snappik.pushme.engine.IActionEvent
import app.snappik.pushme.model.Cell
import app.snappik.pushme.ui.base.AbstractDialogFragment

class CellsDialogFragment(
        private val isSolution: Boolean,
        private val onCellChoose: (cell: Cell) -> Unit
) : AbstractDialogFragment<DialogCellsBinding>(
        R.layout.dialog_cells,
        R.style.SimpleDialog
), IActionEvent {

    private var amount: Int = 1

    override fun DialogCellsBinding.initVariables() {
        dialog = this@CellsDialogFragment
        isSolution = this@CellsDialogFragment.isSolution
        layoutManager = GridLayoutManager(requireContext(), 6, GridLayoutManager.VERTICAL, false)
        dialogCellsAdapter = DialogCellsAdaptor(generateCells(), this@CellsDialogFragment)
    }

    private fun generateCells(): MutableList<Cell>{
        return if(isSolution) mutableListOf(
                Cell.Activator(amount, Cell.Direction.TOP),
                Cell.Activator(amount, Cell.Direction.BOTTOM),
                Cell.Activator(amount, Cell.Direction.LEFT),
                Cell.Activator(amount, Cell.Direction.RIGHT),
                Cell.Filled()
        ) else mutableListOf(
                Cell.Empty(),
                Cell.ActivatorEmpty(),
                Cell.Redirect(Cell.Direction.RIGHT),
                Cell.Redirect(Cell.Direction.LEFT),
                Cell.Redirect(Cell.Direction.TOP),
                Cell.Redirect(Cell.Direction.BOTTOM),
                Cell.Ice(),
                Cell.Hole(),
                Cell.Void()
        )
    }

    override fun dragFinished(from: Int, to: Int, adapterId: Int) = Unit
    override fun allFilled()  = Unit
    override fun gameOver()  = Unit
    override fun isDragEnabled(): Boolean = false

    override fun onClicked(cell: Cell, position: Int, adapterId: Int){
        onCellChoose(cell)
        dismiss()
    }

    fun onValueChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        amount = progress
        BR.dialogCellsAdapter.updateWith{
            dialogCellsAdapter?.updateAmount(progress)
        }
    }

    class DialogCellsAdaptor(
            private val list: MutableList<out Cell>,
            listener: IActionEvent
    ) : AbstractCellAdapter(list, listener, BR.dialogCellsAdapter) {
        fun updateAmount(amount: Int){
            list.forEachIndexed { index, cell ->
                if(cell is Cell.Activator){
                    cell.counter = amount
                    notifyItemChanged(index)
                }
            }
        }
    }
}