package app.snappik.pushme.ui.splash

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import app.snappik.pushme.R
import app.snappik.pushme.databinding.FragmentSplashBinding
import app.snappik.pushme.model.User
import app.snappik.pushme.observer.IUserObserver
import app.snappik.pushme.ui.base.AbstractFragment
import app.snappik.pushme.ui.main.MainActivity
import app.snappik.pushme.utils.ParseUtils.parseChapter
import app.snappik.pushme.utils.ParseUtils.parseGuid
import app.snappik.pushme.utils.ParseUtils.parseLevel
import app.snappik.pushme.viewmodel.ChapterViewModel
import app.snappik.pushme.viewmodel.GuidViewModel
import app.snappik.pushme.viewmodel.LevelViewModel
import app.snappik.pushme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.BufferedReader
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : AbstractFragment<FragmentSplashBinding>(), IUserObserver {

    @Inject override lateinit var userViewModel: UserViewModel

    @Inject lateinit var chapterViewModel: ChapterViewModel

    @Inject lateinit var levelViewModel: LevelViewModel

    @Inject lateinit var guidViewModel: GuidViewModel

    override val viewId: Int = R.layout.fragment_splash

    override fun FragmentSplashBinding.initVariables(){
        progress = 0
        maxProgress = 100
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToUserUpdate()
    }

    override fun onUserFetched(user: User) {
        ObjectAnimator.ofInt(viewDataBinding.progressBar, "progress",0, 100).setDuration(2000).start()
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(requireContext(), MainActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        }, 2000)
    }

    override fun noUser() {
        val chapters = requireActivity().assets.list(chaptersFolder)
        CoroutineScope(Dispatchers.IO).launch {
            loadChapters(chapters)
            loadGuides()
            userViewModel.insert(User())
        }
    }

    private suspend fun loadChapters(chapters: Array<String>?){
        chapters
            ?.map { Pair(it, it.parseChapter()) }
            ?.sortedBy { it.second.order }
            ?.forEachIndexed { index, (fileName, chapter) ->
                chapterViewModel.insertChapter(chapter)
                requireContext()
                    .assets
                    .open("$chaptersFolder/$fileName")
                    .bufferedReader()
                    .use(BufferedReader::readText)
                    .parseLevel(index + 1)
                    .forEach { levelViewModel.insertLevel(it) }
            }
    }

    private suspend fun loadGuides(){
        requireContext()
                .assets
                .open(guides)
                .bufferedReader()
                .use(BufferedReader::readText)
                .parseGuid()
                .forEach { guidViewModel.insertGuid(it) }

    }

    companion object {
        private const val chaptersFolder = "chapter"
        private const val guides = "guides"
    }
}