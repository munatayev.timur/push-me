package app.snappik.pushme.model

import kotlin.math.sqrt

sealed class Cell(val ordinal: Int){

    abstract class Fillable(ordinal: Int): Cell(ordinal)
    abstract class Store(ordinal: Int): Solution(ordinal)
    abstract class Solution(ordinal: Int): Cell(ordinal)

    class Filled: Store(3) {
        override fun hashCode(): Int = ordinal
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            return true
        }
    }
    class Empty: Fillable(2)
    class ActivatorEmpty : Cell(1)
    class Void: Cell(0)
    class Activator(var counter: Int = 0, val direction: Direction = Direction.TOP): Solution(4){
        override fun hashCode(): Int = "$counter${direction.ordinal}".toInt()
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            other as Activator
            if (counter != other.counter || direction != other.direction) return false
            return true
        }
    }
    class Redirect(val direction: Direction): Fillable(5)
    class Ice: Fillable(6)
    class Hole: Cell(7)

    enum class Direction{
        LEFT, RIGHT, TOP, BOTTOM;
        fun getNext(all: Int, current: Int): Int{
            val row = sqrt(all.toDouble()).toInt()
            return when(this){
                LEFT -> current - 1 + if((current) % row == 0) row else 0
                RIGHT -> current + 1 - if((current + 1) % row == 0) row else 0
                TOP -> current - row + if(current - row < 0) all else 0
                BOTTOM -> current + row - if(current + row >= all) all else 0
            }
        }
    }
}