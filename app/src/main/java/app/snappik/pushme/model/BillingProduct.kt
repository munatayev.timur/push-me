package app.snappik.pushme.model

import com.android.billingclient.api.SkuDetails

data class BillingProduct(
    val original: SkuDetails,
    val price: String,
    val title: String
)