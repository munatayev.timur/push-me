package app.snappik.pushme.model

import androidx.room.Entity
import androidx.room.PrimaryKey

fun chapter(block: Chapter.() -> Unit): Chapter = Chapter().apply(block)

@Entity(tableName = "chapter")
data class Chapter(
        var name: String = "",
        var cost: Int = 0,
        var order: Int = 0,
        var isBought: Boolean = false,
        var topColorHex: String = "",
        var bottomColorHex: String = "",
        @PrimaryKey(autoGenerate = true)
        val id:Long = 0
)