package app.snappik.pushme.model

import androidx.room.Entity
import androidx.room.PrimaryKey

fun guid(block: Guid.() -> Unit): Guid = Guid().apply(block)

@Entity(tableName = "guid")
data class Guid(
        @PrimaryKey(autoGenerate = true)
        var id:Long = 0,
        var chapterId: Int = 0,
        var level: Long = 0,
        var startView: Int = 0,
        var endView: Int = 0,
        var text: GuidText = GuidText.DROP,
        var adapter: Int = 0
)