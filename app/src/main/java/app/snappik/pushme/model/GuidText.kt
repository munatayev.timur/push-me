package app.snappik.pushme.model

import app.snappik.pushme.R

enum class GuidText(val res: Int) {
    DROP(R.string.guid_drop),
    JUMP(R.string.guid_jump),
    BORDER(R.string.guid_border),
    Filled(R.string.guid_filled),
    INVENTORY_FILLED(R.string.guid_filled_inventory),//todo not done
    DIRECTION(R.string.guid_direction),
    ICE(R.string.guid_ice),
    HOLE(R.string.guid_hole)//todo not done
}