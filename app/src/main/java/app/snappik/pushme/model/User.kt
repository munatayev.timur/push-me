package app.snappik.pushme.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
        @PrimaryKey(autoGenerate = true)
        val id:Long = 0,
        var coins: Int = 0,
        var inventory: String = "",
        var isFirstBuy: Boolean = true
)