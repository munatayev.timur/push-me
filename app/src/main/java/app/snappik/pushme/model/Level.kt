package app.snappik.pushme.model

import androidx.room.Entity
import androidx.room.PrimaryKey

fun level(block: Level.() -> Unit): Level = Level().apply(block)

@Entity(tableName = "level")
data class Level(
        @PrimaryKey(autoGenerate = true)
        val id:Long = 0,
        var index: Int = 0,
        var chapterId: Int = -1,
        var isDone: Boolean = false,
        var path: String = "",
        var solution: String = "",
        var coins: Int = 0,
        var isLast: Boolean = false,
        var lastEntering: Long = 0
)