package app.snappik.pushme.view

import android.view.View
import app.snappik.pushme.engine.IActionEvent
import app.snappik.pushme.model.Cell
import app.snappik.pushme.utils.animateCellInit
import app.snappik.pushme.utils.animateChanges
import kotlin.reflect.KClass


class ActivatorEmptyViewHolder(view: View,
                               listener: IActionEvent,
                               activatorId: Int) : IViewHolder(view, listener, activatorId), IDragListener {
    override val mItemView: View = view
    override val dragFor: List<KClass<out Cell>> = listOf(Cell.Activator::class)
    fun bind(){
        animateCellInit(itemView)
        registerDropListener(this)
    }
}