package app.snappik.pushme.view

import android.view.View
import android.widget.TextView
import app.snappik.pushme.R
import app.snappik.pushme.engine.IActionEvent
import app.snappik.pushme.model.Cell
import app.snappik.pushme.utils.animateCellInit

class FilledViewHolder(itemView: View,
                       listener: IActionEvent,
                       adapterId: Int) : IViewHolder(itemView, listener, adapterId) {
    fun bind(counter: Int = 1){
        animateCellInit(itemView)
        itemView.findViewById<TextView>(R.id.item_a_count).text = if(counter > 1) "x$counter" else ""
        registerDragListener(Cell.Filled::class)
    }
}