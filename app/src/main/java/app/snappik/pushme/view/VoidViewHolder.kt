package app.snappik.pushme.view

import android.view.View
import app.snappik.pushme.engine.IActionEvent

class VoidViewHolder(itemView: View,
                     listener: IActionEvent,
                     adapterId: Int) : IViewHolder(itemView, listener, adapterId)