package app.snappik.pushme.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.setMargins
import androidx.databinding.BindingAdapter
import app.snappik.pushme.R
import app.snappik.pushme.databinding.LayoutKingBinding
import app.snappik.pushme.ui.base.AbstractLayoutDataBinding
import app.snappik.pushme.utils.getRandomEmotion
import java.util.*

class KingLayout : AbstractLayoutDataBinding<LayoutKingBinding>, MotionLayout.TransitionListener {

    constructor(context: Context) : super(context) { init() }

    constructor(context: Context, attr: AttributeSet) : super(context, attr) { init() }

    constructor(context: Context, attr: AttributeSet, i: Int) : super(context, attr, i) { init() }

    override fun getViewId(): Int = R.layout.layout_king

    override fun LayoutKingBinding.initVariables(){
        isIntroduction = mIsIntroduction
    }

    private fun init(){
        findViewById<MotionLayout>(R.id.king_emotion).addTransitionListener(this)
    }

    private val list = LinkedList<Emotion>()

    fun showEmotion(vararg emotion: Emotion) {
        list.addAll(emotion)
        findViewById<MotionLayout>(R.id.king_emotion).apply {
            val mEmotion = list.pop()
            setTransition(R.id.king_start, mEmotion.id)
            setTransitionDuration(mEmotion.delay)
            transitionToEnd()
        }
    }

    override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) = Unit
    override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) = Unit
    override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) = Unit
    override fun onTransitionCompleted(p0: MotionLayout?, transition: Int) {
        if(transition == R.id.king_start){
            findViewById<MotionLayout>(R.id.king_emotion).apply {
                val mEmotion = list.pop()
                setTransition(R.id.king_start, mEmotion.id)
                setTransitionDuration(mEmotion.delay)
                transitionToEnd()
            }
        } else if(list.isNotEmpty() ){
            findViewById<MotionLayout>(R.id.king_emotion).apply {
                setTransitionDuration(1)
                transitionToState(R.id.king_start)
            }
        } else if(list.isEmpty() && randomize){
            findViewById<MotionLayout>(R.id.king_emotion).apply {
                list.add(getRandomEmotion())
                setTransitionDuration(1)
                transitionToState(R.id.king_start)
            }
        }
    }

    private var randomize = false

    fun setRandomEmotions(isRun : Boolean){
        randomize = isRun
        post {
            if(isRun) showEmotion(getRandomEmotion())
        }
    }

    private var mIsIntroduction = false

    fun setIsIntroduction(isIntroduction: Boolean){
        this.mIsIntroduction = isIntroduction
    }

    class Emotion(val id: Int, val delay: Int)
}

@BindingAdapter("layoutMargin")
fun setLayoutMargin(view: View, dimen: Float) {
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.setMargins(dimen.toInt())
    view.layoutParams = layoutParams
}

@BindingAdapter("layoutMarginRightEye")
fun setLayoutMarginRightEye(view: View, dimen: Float) {
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.setMargins(0, dimen.toInt(), dimen.toInt(), dimen.toInt())
    view.layoutParams = layoutParams
}

@BindingAdapter("layoutMarginLeftEye")
fun setLayoutMarginLeftEye(view: View, dimen: Float) {
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.setMargins(dimen.toInt(), dimen.toInt(), 0, dimen.toInt())
    view.layoutParams = layoutParams
}