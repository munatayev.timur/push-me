package app.snappik.pushme.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import app.snappik.pushme.BR
import app.snappik.pushme.R
import app.snappik.pushme.databinding.LayoutGeneralBinding
import app.snappik.pushme.model.Cell
import app.snappik.pushme.ui.base.AbstractLayoutDataBinding

class GeneralItemView : AbstractLayoutDataBinding<LayoutGeneralBinding> {

    constructor(context: Context) : super(context)

    constructor(context: Context, attr: AttributeSet) : super(context, attr)

    constructor(context: Context, attr: AttributeSet, i: Int) : super(context, attr, i)

    override fun getViewId(): Int = R.layout.layout_general

    override fun LayoutGeneralBinding.initVariables() = Unit

    fun setData(pair: Pair<Int, Cell.Store?>?){
        BR.generalText.updateWith { generalText = pair?.first.toString() }
        val res = when(pair?.second){
            is Cell.Filled -> R.layout.item_filled
            else -> R.layout.item_coins
        }
        val view = LayoutInflater.from(context).inflate(res, null)
        findViewById<FrameLayout>(R.id.imageContainer).apply {
            removeAllViews()
            addView(view)
        }
    }
}