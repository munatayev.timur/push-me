package app.snappik.pushme.view

import android.view.View
import app.snappik.pushme.engine.IActionEvent
import app.snappik.pushme.model.Cell
import app.snappik.pushme.utils.animateCellInit
import kotlin.reflect.KClass

class EmptyViewHolder(override val mItemView: View,
                      listener: IActionEvent,
                      adapterId: Int) : IViewHolder(mItemView, listener, adapterId), IDragListener {
    override val dragFor: List<KClass<out Cell>> = listOf(Cell.Filled::class)
    fun bind(){
        animateCellInit(itemView)
        registerDropListener(this)
    }
}