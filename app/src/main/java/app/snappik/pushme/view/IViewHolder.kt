package app.snappik.pushme.view

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipDescription
import android.content.Intent
import android.os.Bundle
import android.view.DragEvent
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import app.snappik.pushme.engine.IActionEvent
import app.snappik.pushme.model.Cell
import kotlin.math.abs
import kotlin.reflect.KClass


abstract class IViewHolder(itemView: View,
                           private val actionListener: IActionEvent,
                           private val adapterId: Int) : RecyclerView.ViewHolder(itemView) {

    private var startInteraction = 0F

    @SuppressLint("ClickableViewAccessibility")
    protected fun registerDragListener(cellType: KClass<out Cell.Solution>) {
        if(actionListener.isDragEnabled().not()) return
        itemView.setOnTouchListener { view, motionEvent ->
            if(motionEvent.action == MotionEvent.ACTION_DOWN && startInteraction == 0F){
                startInteraction = motionEvent.y
                false
            } else if(motionEvent.action == MotionEvent.ACTION_MOVE
                && abs(startInteraction - motionEvent.y) > DRAG_DELTA
                && startInteraction != 0F){
                startInteraction = 0F
                val bundle = Bundle().apply {
                    putInt(POSITION, adapterPosition)
                    putInt(ADAPTER_ID, adapterId)
                }
                val intent = Intent().apply { putExtras(bundle) }
                val mimeTypes = arrayOf(ClipDescription.MIMETYPE_TEXT_INTENT)
                val data = ClipData(cellType.simpleName, mimeTypes, ClipData.Item(intent))
                val shadowBuilder = View.DragShadowBuilder(view)
                view.alpha = 0.3F
                view.startDragAndDrop(data, shadowBuilder, view, 2)
                true
            } else false
        }
    }

    protected fun registerDropListener(listener: IDragListener) {
        if(actionListener.isDragEnabled().not()) return
        itemView.setOnDragListener { _, event ->
            when (event.action) {
                DragEvent.ACTION_DRAG_STARTED -> listener.isOurClient(event){ onDragStarted() }
                DragEvent.ACTION_DRAG_ENTERED -> listener.isOurClient(event){ onDragEntered() }
                DragEvent.ACTION_DRAG_EXITED -> listener.isOurClient(event){ onDragExited() }
                DragEvent.ACTION_DROP -> listener.isOurClient(event){
                    event.clipData.getItemAt(0).intent.extras?.let { extras ->
                        val position = extras.getInt(POSITION)
                        val adapterId = extras.getInt(ADAPTER_ID)
                        actionListener.dragFinished(position, adapterPosition, adapterId)
                        mItemView.setOnDragListener(null)
                    }
                }
                DragEvent.ACTION_DRAG_ENDED -> {
                    val view = event.localState as? View
                    view?.alpha = 1F
                    listener.onDragCanceled()
                }
                else -> Unit
            }
            true
        }
    }

    private fun IDragListener.isOurClient(dragEvent: DragEvent, ourClient: IDragListener.() -> Unit){
        val cellType = dragEvent.clipDescription?.label
        if(dragFor.map { it.simpleName }.contains(cellType)) ourClient.invoke(this)
    }

    companion object {
        const val POSITION = "POSITION"
        const val ADAPTER_ID = "ADAPTER_ID"
        private const val DRAG_DELTA = 30F
    }
}