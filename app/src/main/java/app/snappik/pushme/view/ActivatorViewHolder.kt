package app.snappik.pushme.view

import android.view.View
import android.widget.TextView
import app.snappik.pushme.R
import app.snappik.pushme.engine.IActionEvent
import app.snappik.pushme.model.Cell
import app.snappik.pushme.utils.animateCellInit

class ActivatorViewHolder(itemView: View,
                          listener: IActionEvent,
                          adapterId: Int) : IViewHolder(itemView, listener, adapterId) {
    fun bind(cell: Cell.Activator, counter: Int = 1) {
        animateCellInit(itemView)
        itemView.findViewById<View>(R.id.item_a_card).rotation = when(cell.direction){
            Cell.Direction.LEFT -> 90F
            Cell.Direction.RIGHT -> 270F
            Cell.Direction.TOP -> 180F
            Cell.Direction.BOTTOM -> 0F
        }
        itemView.findViewById<TextView>(R.id.item_a_text).apply {
            text = cell.counter.toString()
            rotation = when(cell.direction){
                Cell.Direction.LEFT -> 270F
                Cell.Direction.RIGHT -> 90F
                Cell.Direction.TOP -> 180F
                Cell.Direction.BOTTOM -> 0F
            }
        }
        itemView.findViewById<TextView>(R.id.item_a_count).text = if(counter > 1) "x$counter" else ""
        registerDragListener(Cell.Activator::class)
    }
}