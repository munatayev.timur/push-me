package app.snappik.pushme.view

import android.view.View
import app.snappik.pushme.R
import app.snappik.pushme.engine.IActionEvent
import app.snappik.pushme.model.Cell
import app.snappik.pushme.utils.animateCellInit
import kotlin.reflect.KClass

class RedirectViewHolder(override val mItemView: View,
                         listener: IActionEvent,
                         adapterId: Int) : IViewHolder(mItemView, listener, adapterId), IDragListener {
    override val dragFor: List<KClass<out Cell>> = listOf(Cell.Filled::class)
    fun bind(cell: Cell.Redirect){
        animateCellInit(itemView)
        registerDropListener(this)
        animateCellInit(itemView)
        itemView.findViewById<View>(R.id.item_redirect_dir).rotation = when(cell.direction){
            Cell.Direction.LEFT -> 180F
            Cell.Direction.RIGHT -> 0F
            Cell.Direction.TOP -> 270F
            Cell.Direction.BOTTOM -> 90F
        }
    }
}