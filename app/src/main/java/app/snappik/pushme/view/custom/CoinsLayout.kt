package app.snappik.pushme.view.custom

import android.content.Context
import android.util.AttributeSet
import app.snappik.pushme.BR
import app.snappik.pushme.R
import app.snappik.pushme.databinding.LayoutCoinsBinding
import app.snappik.pushme.ui.base.AbstractLayoutDataBinding

class CoinsLayout : AbstractLayoutDataBinding<LayoutCoinsBinding> {

    constructor(context: Context) : super(context)

    constructor(context: Context, attr: AttributeSet) : super(context, attr)

    constructor(context: Context, attr: AttributeSet, i: Int) : super(context, attr, i)

    override fun getViewId(): Int = R.layout.layout_coins

    override fun LayoutCoinsBinding.initVariables() = Unit

    fun setCoins(coins: Int) {
        BR.layoutCoinsText.updateWith { layoutCoinsText = coins }
    }

    fun setOpenOnClick(runnable: Runnable){
        BR.layoutOpenCoinsRunnable.updateWith { layoutOpenCoinsRunnable = runnable }
    }
}