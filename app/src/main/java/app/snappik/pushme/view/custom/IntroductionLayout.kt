package app.snappik.pushme.view.custom

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.widget.TextView
import app.snappik.pushme.R
import app.snappik.pushme.databinding.LayoutIntroductionBinding
import app.snappik.pushme.ui.base.AbstractLayoutDataBinding

class IntroductionLayout : AbstractLayoutDataBinding<LayoutIntroductionBinding> {

    constructor(context: Context) : super(context) { init() }

    constructor(context: Context, attr: AttributeSet) : super(context, attr) { init() }

    constructor(context: Context, attr: AttributeSet, i: Int) : super(context, attr, i) { init() }

    override fun getViewId(): Int = R.layout.layout_introduction

    override fun LayoutIntroductionBinding.initVariables() = Unit

    private var finishRunnable: Runnable? = null

    private val mHandler = Handler(Looper.getMainLooper())

    private val textList = listOf(
        context.getString(R.string.intro_1),
        context.getString(R.string.intro_2),
        context.getString(R.string.intro_3),
        context.getString(R.string.intro_4),
        context.getString(R.string.intro_5),
        context.getString(R.string.intro_6)
    )

    private fun init(){
        val allEmotions = listOf(
            KingLayout.Emotion(R.id.king_surprise, 5000),
            KingLayout.Emotion(R.id.king_idle, 5000),
            KingLayout.Emotion(R.id.king_idle, 5000),
            KingLayout.Emotion(R.id.king_idle, 5000),
            KingLayout.Emotion(R.id.king_funny, 5000),
            KingLayout.Emotion(R.id.king_idle, 5000)
        )
        findViewById<KingLayout>(R.id.kingLayout).post {
            findViewById<KingLayout>(R.id.kingLayout).showEmotion(*allEmotions.toTypedArray())
        }
        textList.forEachIndexed { index, text ->
            val delay = index * allEmotions[index].delay.toLong()
            mHandler.postDelayed({
                findViewById<TextView>(R.id.introduction_text).text = text
                if (index == textList.size - 1){
                    finishRunnable?.run()
                }
            }, delay)
        }
    }

    fun setFinish(finishRunnable: Runnable){
        this.finishRunnable = finishRunnable
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mHandler.removeCallbacksAndMessages(null)
        finishRunnable = null
    }
}