package app.snappik.pushme.view

import android.graphics.Color
import android.view.View
import app.snappik.pushme.model.Cell
import kotlin.reflect.KClass

interface IDragListener {

    val mItemView: View

    val dragFor: List<KClass<out Cell>>

    fun onDragStarted(){
        mItemView.setBackgroundColor(Color.YELLOW)
    }

    fun onDragEntered(){
        mItemView.setBackgroundColor(Color.GREEN)
    }

    fun onDragExited(){
        mItemView.setBackgroundColor(Color.YELLOW)
    }

    fun onDragCanceled(){
        mItemView.setBackgroundColor(Color.TRANSPARENT)
    }
}