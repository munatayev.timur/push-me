package app.snappik.pushme.view.guid

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.snappik.pushme.R
import app.snappik.pushme.databinding.ViewGuidBinding
import app.snappik.pushme.model.GuidText
import app.snappik.pushme.ui.base.AbstractLayoutDataBinding
import app.snappik.pushme.view.ActivatorEmptyViewHolder
import app.snappik.pushme.view.ActivatorViewHolder
import app.snappik.pushme.view.EmptyViewHolder
import app.snappik.pushme.view.FilledViewHolder
import app.snappik.pushme.view.HoleViewHolder
import app.snappik.pushme.view.IceViewHolder
import app.snappik.pushme.view.RedirectViewHolder

class GuidLayout : AbstractLayoutDataBinding<ViewGuidBinding> {

    private val addedView = mutableListOf<View>()

    constructor(context: Context) : super(context)

    constructor(context: Context, attr: AttributeSet) : super(context, attr)

    constructor(context: Context, attr: AttributeSet, i: Int) : super(context, attr, i)

    override fun getViewId(): Int = R.layout.view_guid

    override fun ViewGuidBinding.initVariables() = Unit

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        removeAddedViews()
        visibility = View.GONE
        return super.onTouchEvent(event)
    }

    fun showDrag(target: RecyclerView.ViewHolder, destination: RecyclerView.ViewHolder, guid: app.snappik.pushme.model.Guid) {
        var fromBottom = true
        val destinationCoordinates = IntArray(2).also { destination.itemView.getLocationInWindow(it) }
        var textCoordinates = destinationCoordinates[1].toFloat() / Resources.getSystem().displayMetrics.heightPixels.toFloat()
        when (guid.text) {
            GuidText.DROP, GuidText.JUMP, GuidText.BORDER, GuidText.Filled -> startDragAnimation(target, destination, destinationCoordinates)
            GuidText.DIRECTION, GuidText.ICE, GuidText.HOLE -> startShowAnimation(destination, destinationCoordinates)
            GuidText.INVENTORY_FILLED -> {
                fromBottom = false
                val targetCoordinates = IntArray(2).also { target.itemView.getLocationInWindow(it) }
                textCoordinates = targetCoordinates[1].toFloat() / Resources.getSystem().displayMetrics.heightPixels.toFloat()
                startShowAnimation(target, targetCoordinates)
            }
        }
        locateText(context.getString(guid.text.res), textCoordinates, fromBottom)
        findViewById<View>(R.id.guidRoot)?.visibility = View.VISIBLE
    }

    private fun locateText(mText: String, bias: Float, fromBottom: Boolean = true) {
        findViewById<TextView>(R.id.guidText)?.apply {
            visibility = View.VISIBLE
            text = mText
            val delta = 0.1F + 0.02F * lineCount * if (fromBottom) 1F else -1.5F
            layoutParams = (layoutParams as ConstraintLayout.LayoutParams).apply { verticalBias = (bias + delta) }
        }
    }

    private fun startShowAnimation(destination: RecyclerView.ViewHolder?, destinationCoordinates: IntArray) {
        destination?.cloneView(destinationCoordinates)
                .also { addView(it) }
    }

    private fun startDragAnimation(target: RecyclerView.ViewHolder?,
                                   destination: RecyclerView.ViewHolder?,
                                   destinationCoordinates: IntArray) {
        val targetCoordinates = IntArray(2)
                .also { target?.itemView?.getLocationInWindow(it) }
        destination?.cloneView(destinationCoordinates)
                .also { addViewAbove(it) }
        target?.cloneView(targetCoordinates)
                .also { addViewAbove(it) }
                ?.repeatableAnimation(targetCoordinates, destinationCoordinates)
    }

    private fun addViewAbove(view: View?) = view?.let {
        addedView.add(it)
        addView(it)
    }

    private fun removeAddedViews() {
        findViewById<View>(R.id.guidRoot)?.visibility = View.INVISIBLE
        findViewById<TextView>(R.id.guidText).visibility = INVISIBLE
        addedView.forEach { removeView(it) }
        addedView.clear()
    }

    private fun View.repeatableAnimation(initial: IntArray, coordinates: IntArray) {
        animate()
                .x(coordinates[0].toFloat())
                .y(coordinates[1].toFloat())
                .setInterpolator(DecelerateInterpolator(2F))
                .setDuration(2000)
                .withEndAction {
                    x = initial[0].toFloat()
                    y = initial[1].toFloat()
                    this.repeatableAnimation(initial, coordinates)
                }
                .start()
    }

    private fun RecyclerView.ViewHolder.cloneView(targetCoordinates: IntArray): View {
        return when (this) {
            is ActivatorViewHolder -> context.inflate(R.layout.item_activator).apply {
                val sourceText = itemView.findViewById<TextView>(R.id.item_a_text)
                val sourceCard = itemView.findViewById<View>(R.id.item_a_card)
                findViewById<TextView>(R.id.item_a_text).apply {
                    rotation = sourceText.rotation
                    text = sourceText.text
                }
                findViewById<View>(R.id.item_a_card).apply {
                    rotation = sourceCard.rotation
                }
            }
            is ActivatorEmptyViewHolder -> context.inflate(R.layout.item_activator_empty)
            is FilledViewHolder -> context.inflate(R.layout.item_filled)
            is EmptyViewHolder -> context.inflate(R.layout.item_empty)
            is RedirectViewHolder -> context.inflate(R.layout.item_redirect).apply {
                val sourceCard = itemView.findViewById<View>(R.id.item_a_card)
                findViewById<ConstraintLayout>(R.id.item_redirect_dir).rotation = sourceCard.rotation
            }
            is IceViewHolder -> context.inflate(R.layout.item_ice)
            is HoleViewHolder -> context.inflate(R.layout.item_hole)
            else -> context.inflate(R.layout.item_activator)
        }.apply {
            x = targetCoordinates[0].toFloat()
            y = targetCoordinates[1].toFloat()
            layoutParams = this@cloneView.itemView.layoutParams
        }
    }

    private fun Context.inflate(id: Int) = LayoutInflater.from(this).inflate(id, null)

    companion object {
        const val WAIT_BEFORE_GUID = 2000L
    }
}