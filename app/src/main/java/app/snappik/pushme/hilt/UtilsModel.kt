package app.snappik.pushme.hilt

import android.app.Application
import androidx.fragment.app.FragmentActivity
import app.snappik.pushme.utils.BillingManager
import app.snappik.pushme.utils.EmailSender
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import java.util.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UtilsModel {

    @Singleton
    @Provides
    fun provideSender(app: Application, properties: Properties) = EmailSender(app, properties)

    @Singleton
    @Provides
    fun provideProperties(app: Application) = Properties().apply {
        load(app.assets.open(PROP_NAME))
    }

    @Singleton
    @Provides
    fun provideBillingManager(app: Application) = BillingManager(app)

    private const val PROP_NAME = "app.properties"
}