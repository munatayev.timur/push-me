package app.snappik.pushme.hilt

import android.content.Context
import app.snappik.pushme.database.LocalDatabase
import app.snappik.pushme.database.dao.ChapterDao
import app.snappik.pushme.database.dao.GuidDao
import app.snappik.pushme.database.dao.LevelDao
import app.snappik.pushme.database.dao.UserDao
import app.snappik.pushme.database.repository.ChapterRepository
import app.snappik.pushme.database.repository.GuidRepository
import app.snappik.pushme.database.repository.LevelRepository
import app.snappik.pushme.database.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModel {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = LocalDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideUserDao(db: LocalDatabase) = db.userDao

    @Singleton
    @Provides
    fun provideUserRepository(userDao: UserDao) = UserRepository(userDao)

    @Singleton
    @Provides
    fun provideChapterDao(db: LocalDatabase) = db.chapterDao

    @Singleton
    @Provides
    fun provideChapterRepository(chapterDao: ChapterDao) = ChapterRepository(chapterDao)

    @Singleton
    @Provides
    fun provideLevelDao(db: LocalDatabase) = db.levelDao

    @Singleton
    @Provides
    fun provideGuidDao(db: LocalDatabase) = db.guidDao

    @Singleton
    @Provides
    fun provideGuidRepository(guidDao: GuidDao) = GuidRepository(guidDao)

    @Singleton
    @Provides
    fun provideLevelRepository(levelDao: LevelDao) = LevelRepository(levelDao)
}