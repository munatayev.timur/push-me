package app.snappik.pushme.hilt

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.snappik.pushme.database.repository.ChapterRepository
import app.snappik.pushme.database.repository.GuidRepository
import app.snappik.pushme.database.repository.LevelRepository
import app.snappik.pushme.database.repository.UserRepository
import app.snappik.pushme.viewmodel.ChapterViewModel
import app.snappik.pushme.viewmodel.GuidViewModel
import app.snappik.pushme.viewmodel.LevelViewModel
import app.snappik.pushme.viewmodel.UserViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ViewModel {

    @Singleton
    @Provides
    fun provideUserViewModel(app: Application, userRepository: UserRepository): UserViewModel =
        app.create(UserViewModel::class.java).apply { this.userRepository = userRepository }

    @Singleton
    @Provides
    fun provideChapterViewModel(app: Application, chapterRepository: ChapterRepository): ChapterViewModel =
        app.create(ChapterViewModel::class.java).apply { this.chapterRepository = chapterRepository }

    @Singleton
    @Provides
    fun provideLevelViewModel(app: Application, levelRepository: LevelRepository): LevelViewModel =
        app.create(LevelViewModel::class.java).apply { this.levelRepository = levelRepository }

    @Singleton
    @Provides
    fun provideGuidViewModel(app: Application, guidRepository: GuidRepository): GuidViewModel =
            app.create(GuidViewModel::class.java).apply { this.guidRepository = guidRepository }

    private fun <T : ViewModel> Application.create(kk: Class<T>) = ViewModelProvider.AndroidViewModelFactory.getInstance(this).create(kk)
}