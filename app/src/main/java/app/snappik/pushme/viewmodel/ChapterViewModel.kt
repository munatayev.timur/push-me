package app.snappik.pushme.viewmodel

import androidx.lifecycle.LiveData
import app.snappik.pushme.database.repository.ChapterRepository
import app.snappik.pushme.model.Chapter
import kotlinx.coroutines.launch

class ChapterViewModel : AbstractViewModel() {
    var chapters: LiveData<List<Chapter>>? = null

    var chapterRepository: ChapterRepository? = null
        set(value) {
            field = value
            chapters = value?.getChapters()
        }

    suspend fun insertChapter(chapter: Chapter) = chapterRepository?.insertChapter(chapter)

    fun buyChapter(chapterId: Long) = dbScope.launch { chapterRepository?.buyChapter(chapterId) }
}