package app.snappik.pushme.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.snappik.pushme.database.repository.LevelRepository
import app.snappik.pushme.model.Level
import kotlinx.coroutines.launch

class LevelViewModel : AbstractViewModel() {

    private val mAddCounter = MutableLiveData(5)
    val addCounter: LiveData<Int> = mAddCounter

    var levelRepository: LevelRepository? = null

    fun getLevelsByChapterId(chapterId: Long) = levelRepository?.getLevelsByChapterId(chapterId)

    fun getLevelById(levelId: Long) = levelRepository?.getLevelById(levelId)

    fun setAsLastActive(levelId: Long) = dbScope.launch { levelRepository?.setLastActive(levelId) }

    fun setPath(levelId: Long, path: String) = dbScope.launch { levelRepository?.updatePathByLevelId(path, levelId) }

    fun setSolution(levelId: Long, solution: String) = dbScope.launch { levelRepository?.updateSolutionByLevelId(solution, levelId) }

    fun getLastActiveLevel()= levelRepository?.getLastActiveLevel()

    fun addLevel() = dbScope.launch {
        levelRepository?.countLevels(-1)?.let {
            insertLevel(Level(isLast = true, index = it + 1))
        }
    }

    fun deleteLevel(level: Level) = dbScope.launch { levelRepository?.delete(level) }

    suspend fun insertLevel(level: Level) = levelRepository?.insert(level)

    fun updateDoneByLevelId(levelId: Long) = dbScope.launch {
        levelRepository?.updateDoneByLevelId(levelId)
    }

    fun updateCounter(){
        mAddCounter.postValue(if(addCounter.value == 0) 5 else (addCounter.value ?: 5) - 1)
    }
}