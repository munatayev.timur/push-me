package app.snappik.pushme.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.snappik.pushme.database.repository.UserRepository
import app.snappik.pushme.model.User
import app.snappik.pushme.viewmodel.transformations.getCoins
import kotlinx.coroutines.launch

class UserViewModel : AbstractViewModel() {

    var user: LiveData<User>? = MutableLiveData()

    var userRepository: UserRepository? = null
        set(value) {
            field = value
            user = value?.getUser()
        }

    fun getCoins() = user?.getCoins()

    suspend fun insert(user: User) = userRepository?.insert(user)

    fun addToInventory(items: String) = dbScope.launch {
        user?.value?.let {
            userRepository?.update(it.apply {
                inventory += if(inventory.isNotEmpty()) ",$items" else items
            })
        }
    }

    fun saveInventory(items: String) = dbScope.launch {
        user?.value?.let { userRepository?.update(it.apply { inventory = items}) }
    }

    fun updateCoins(mCoins: Int) = dbScope.launch {
        user?.value?.let { userRepository?.update(it.apply { coins += mCoins }) }
    }

    fun madePurchase() = dbScope.launch {
        user?.value?.let { userRepository?.update(it.apply { isFirstBuy = false }) }
    }
}