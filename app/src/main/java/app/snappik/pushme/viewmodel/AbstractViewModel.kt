package app.snappik.pushme.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

abstract class AbstractViewModel : ViewModel() {
    protected val dbScope = CoroutineScope(Dispatchers.IO)
    protected val mainScope = CoroutineScope(Dispatchers.Main)
}