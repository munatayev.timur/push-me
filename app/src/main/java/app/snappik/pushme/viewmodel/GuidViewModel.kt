package app.snappik.pushme.viewmodel

import app.snappik.pushme.database.repository.GuidRepository
import app.snappik.pushme.model.Guid

class GuidViewModel : AbstractViewModel() {

    var guidRepository: GuidRepository? = null

    fun getGuidByLevel(id: Int, chapterId: Int) = guidRepository?.getGuidByLevel(id, chapterId)

    suspend fun insertGuid(guid: Guid) = guidRepository?.insert(guid)
}