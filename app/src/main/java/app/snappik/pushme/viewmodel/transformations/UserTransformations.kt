package app.snappik.pushme.viewmodel.transformations

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import app.snappik.pushme.model.User

fun LiveData<User>.getCoins(): LiveData<Int> = Transformations.map(this) { user -> user.coins }