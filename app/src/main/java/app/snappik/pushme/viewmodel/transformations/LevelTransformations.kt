package app.snappik.pushme.viewmodel.transformations

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import app.snappik.pushme.model.Level

fun LiveData<List<Level>>.getChapterLevels(): LiveData<String> =
        Transformations.map(this) { list -> "${list.count { it.isDone }}/${list.size}" }