package app.snappik.pushme.engine

import app.snappik.pushme.model.Cell
import app.snappik.pushme.view.ActivatorEmptyViewHolder
import app.snappik.pushme.view.ActivatorViewHolder
import app.snappik.pushme.view.FilledViewHolder
import app.snappik.pushme.view.IViewHolder
import kotlin.reflect.KClass

abstract class BottomAdapter(
        list: MutableList<out Cell.Solution>,
        listener: IActionEvent,
        adapterId: Int
): AbstractCellAdapter(list, listener, adapterId) {

    protected val stackedList = list
            .groupBy { it }
            .map { it.key to it.value.size }
            .associateTo(HashMap(), {it.first to it.second})

    fun isContains(cell: KClass<out Cell.Solution>): Boolean {
        return stackedList.keys.find { it::class == cell } != null
    }

    override fun getItemCount(): Int = stackedList.size

    override fun getItemViewType(position: Int): Int = stackedList.toList()[position].first.ordinal

    fun getByPositionAndRemove(position: Int): Cell{
        val entry = stackedList.toList()[position]
        if(entry.second <= 1){
            stackedList.remove(entry.first)
            notifyItemRemoved(position)
        } else {
            stackedList[entry.first] = entry.second - 1
            notifyItemChanged(position)
        }
        return entry.first
    }

    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        val list = stackedList.toList()[position]
        holder.itemView.setOnClickListener { /* do not allow click on bottom */ }
        when(holder){
            is ActivatorViewHolder -> holder.bind(list.first as Cell.Activator, list.second)
            is ActivatorEmptyViewHolder -> holder.bind()
            is FilledViewHolder -> holder.bind(list.second)
        }
    }
}