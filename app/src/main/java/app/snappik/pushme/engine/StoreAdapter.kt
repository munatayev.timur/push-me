package app.snappik.pushme.engine

import app.snappik.pushme.BR
import app.snappik.pushme.model.Cell
import app.snappik.pushme.utils.ParseUtils.parseToStringString
import app.snappik.pushme.view.FilledViewHolder
import app.snappik.pushme.view.IViewHolder

class StoreAdapter(
        list: ArrayList<Cell.Store> = ArrayList(),
        listener: IActionEvent
) : BottomAdapter(list, listener, BR.adapterInventory) {

    fun getAllInventoryToSave(): String{
        val result = ArrayList<Cell.Store>()
        stackedList.forEach {
            for(i in 1..it.value){
                val key = it.key
                if(key is Cell.Store) result.add(key)
            }
        }
        return result.parseToStringString()
    }
}