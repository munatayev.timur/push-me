package app.snappik.pushme.engine

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import app.snappik.pushme.R
import app.snappik.pushme.model.Cell
import app.snappik.pushme.view.ActivatorEmptyViewHolder
import app.snappik.pushme.view.ActivatorViewHolder
import app.snappik.pushme.view.EmptyViewHolder
import app.snappik.pushme.view.FilledViewHolder
import app.snappik.pushme.view.HoleViewHolder
import app.snappik.pushme.view.IViewHolder
import app.snappik.pushme.view.IceViewHolder
import app.snappik.pushme.view.RedirectViewHolder
import app.snappik.pushme.view.VoidViewHolder

abstract class AbstractCellAdapter(
    private val list: MutableList<out Cell> = mutableListOf(),
    private val listener: IActionEvent,
    private val adapterId: Int
) : RecyclerView.Adapter<IViewHolder>() {

    open fun getExistingList() = list

    override fun getItemCount(): Int = list.count()

    override fun getItemViewType(position: Int): Int = list[position].ordinal

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IViewHolder {
        val (holder, layout) = when(viewType){
            1 -> Pair(ActivatorEmptyViewHolder::class.java, R.layout.item_activator_empty)
            2 -> Pair(EmptyViewHolder::class.java, R.layout.item_empty)
            3 -> Pair(FilledViewHolder::class.java, R.layout.item_filled)
            4 -> Pair(ActivatorViewHolder::class.java, R.layout.item_activator)
            5 -> Pair(RedirectViewHolder::class.java, R.layout.item_redirect)
            6 -> Pair(IceViewHolder::class.java, R.layout.item_ice)
            7 -> Pair(HoleViewHolder::class.java, R.layout.item_hole)
            else -> Pair(VoidViewHolder::class.java, R.layout.item_void)
        }
        val width = Resources.getSystem().displayMetrics.widthPixels/8
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
                .apply { layoutParams = ConstraintLayout.LayoutParams(width, width) }
        return holder.getConstructor(View::class.java, IActionEvent::class.java, Int::class.java).newInstance(view, listener, adapterId)
    }

    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        holder.itemView.setOnClickListener { listener.onClicked(list[position], position, adapterId) }
        when(holder){
            is ActivatorViewHolder -> holder.bind(list[position] as Cell.Activator)
            is ActivatorEmptyViewHolder -> holder.bind()
            is RedirectViewHolder -> holder.bind(list[position] as Cell.Redirect)
            is EmptyViewHolder -> holder.bind()
            is IceViewHolder -> holder.bind()
            is HoleViewHolder -> holder.bind()
        }
    }
}