package app.snappik.pushme.engine

import app.snappik.pushme.BR
import app.snappik.pushme.model.Cell

class SolutionAdapter(
        list: MutableList<Cell.Solution> = mutableListOf(),
        listener: IActionEvent
) : BottomAdapter(list, listener, BR.adapterBottom){

    fun addElement(cell: Cell.Solution){
        if(stackedList.containsKey(cell)){
            stackedList[cell] = stackedList[cell]?.plus(1) ?: 1
        } else {
            stackedList[cell] = 1
        }
        notifyDataSetChanged()
    }

    override fun getExistingList(): MutableList<Cell.Solution>{
        val result = mutableListOf<Cell.Solution>()
        stackedList.forEach {
            for(i in 1..it.value){
                val key = it.key
                result.add(key)
            }
        }
        return result
    }
}