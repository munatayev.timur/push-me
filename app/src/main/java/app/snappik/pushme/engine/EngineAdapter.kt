package app.snappik.pushme.engine

import android.os.Handler
import android.os.Looper
import app.snappik.pushme.BR
import app.snappik.pushme.model.Cell

class EngineAdapter(
        private val list: MutableList<Cell> = mutableListOf(),
        private val listener: IActionEvent
) : AbstractCellAdapter(list, listener, BR.adapterMain){

    private val handler = Handler(Looper.getMainLooper())

    fun replace(position: Int, cell: Cell, checkCells: Boolean = true, isFromDialogs: Boolean = false){
        list[position] = if(isFromDialogs) cell else when(list[position]){
            is Cell.Ice -> Cell.Empty()
            is Cell.Hole -> Cell.Hole()
            else -> cell
        }
        notifyItemChanged(position)
        if(checkCells.not()) return
        if(cell is Cell.Activator) fill(position, cell.counter, cell.direction) else checkCells()
    }

    private fun fill(position: Int, count: Int, direction: Cell.Direction, animate: Boolean = true){
        if(count == 0) checkCells()
         else handler.postDelayed({
            val (nextPosition, nextDirection) = getNextPosDirection(direction, position)
            fillCell(position, nextPosition, count, nextDirection)
        }, if(animate) CELL_FILL_ANIMATION_TIME else 0)
    }

    private fun getNextPosDirection(currentDirection: Cell.Direction, currentPosition: Int): Pair<Int, Cell.Direction> {
        val nextPosition = currentDirection.getNext(list.size, currentPosition)
        val nextPlatformCell = list[nextPosition]
        return if(nextPlatformCell is Cell.Redirect) {
            Pair(nextPosition, nextPlatformCell.direction)
        } else {
            Pair(nextPosition, currentDirection)
        }
    }

    private fun fillCell(previousPos: Int, position: Int, count: Int, direction: Cell.Direction){
        when (list[position]) {
            is Cell.Empty, is Cell.Redirect -> {
                list[position] = Cell.Filled()
                notifyItemChanged(position)
                fill(position, count - 1, direction)
            }
            is Cell.Filled -> {
                fill(position, count, direction, false)
            }
            is Cell.Ice -> {
                list[position] = Cell.Empty()
                notifyItemChanged(position)
                fill(previousPos, count - 1, direction)
            }
            is Cell.Hole -> {
                fill(previousPos, count - 1, direction)
            }
            else -> {
                listener.gameOver()
                handler.removeCallbacksAndMessages(null)
            }
        }
    }

    private fun checkCells(){
        if(list.count { it is Cell.Fillable } != 0 && listener.isSolutionContainsFilled()) return
        if((list.count { it is Cell.ActivatorEmpty } == 0 || list.count { it is Cell.Fillable } == 0) && handler.hasMessages(0).not()){
            handler.postDelayed({
                if(list.count { it is Cell.Fillable } == 0){
                    listener.allFilled()
                } else listener.gameOver()
            }, CELL_FILL_ANIMATION_TIME)
        }
    }

    companion object {
        private const val CELL_FILL_ANIMATION_TIME = 400L
    }
}