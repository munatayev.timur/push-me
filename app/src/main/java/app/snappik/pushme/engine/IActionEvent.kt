package app.snappik.pushme.engine

import app.snappik.pushme.model.Cell

interface IActionEvent {
    fun dragFinished(from: Int, to: Int, adapterId: Int)
    fun allFilled()
    fun gameOver()
    fun onClicked(cell: Cell, position: Int, adapterId: Int) = Unit
    fun isDragEnabled() = true
    fun isSolutionContainsFilled() = false
}