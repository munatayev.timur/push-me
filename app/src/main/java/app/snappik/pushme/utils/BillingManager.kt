package app.snappik.pushme.utils

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.snappik.pushme.model.BillingProduct
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.ConsumeParams
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchasesUpdatedListener
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsParams

class BillingManager(val context: Context) {

    var success: (amount: Int) -> Unit = { onError("Billing working incorrectly") }

    private val mProducts = MutableLiveData<List<BillingProduct>>(emptyList())
    val products: LiveData<List<BillingProduct>> = mProducts

    private val purchasesUpdatedListener = PurchasesUpdatedListener { billingResult, purchases ->
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
            purchases?.forEach { handlePurchase(it) }
        } else onError("Can not finnish purchase, something is wrong. Info: ${billingResult.debugMessage}")
    }

    private val billingClient = BillingClient.newBuilder(context)
        .setListener(purchasesUpdatedListener)
        .enablePendingPurchases()
        .build()

    fun establishConnection(isFirstBuy: Boolean){
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                    getAvailableProducts(isFirstBuy)
                } else onError("Connected to billing service, but something is wrong. Info: ${billingResult.debugMessage}")
            }
            override fun onBillingServiceDisconnected() = onError("Can not connect to billing service")
        })
    }

    private fun getAvailableProducts(isFirstBuy: Boolean) = SkuDetailsParams
        .newBuilder()
        .setSkusList(if(isFirstBuy) arrayListOf(PRODUCT_300D, PRODUCT_4000D, PRODUCT_10000D)
        else arrayListOf(PRODUCT_300, PRODUCT_4000, PRODUCT_10000))
        .setType(PRODUCT_TYPE)
        .run {
            billingClient.querySkuDetailsAsync(build()) { billingResp, list ->
                if (billingResp.responseCode == BillingClient.BillingResponseCode.OK && !list.isNullOrEmpty()) {
                    list.sortBy { it.sku.length }
                        list.map { BillingProduct(it, it.price, it.description) }
                        .also { mProducts.postValue(it) }
                }
            }
        }

    private fun handlePurchase(purchase: Purchase?) {
        purchase?.let {
            if (it.purchaseState == Purchase.PurchaseState.PURCHASED) {
                if (!it.isAcknowledged) {
                    consumePurchase(it.purchaseToken, it.sku.convertReward())
                } else success.invoke(it.sku.toInt())
            } else if (it.purchaseState == Purchase.PurchaseState.PENDING) onError("Pending purchase")
            else onError("Can not finish purchase, unknown error")
        } ?: onError("Received empty purchase information")
    }

    private fun consumePurchase(token: String, amount: Int) {
        val consumeParams = ConsumeParams.newBuilder().setPurchaseToken(token).build()
        billingClient.consumeAsync(consumeParams){ result, _ ->
            if (result.responseCode == BillingClient.BillingResponseCode.OK) success.invoke(amount)
            else onError("Purchase not consumed")
        }
    }

    fun purchase(activity: FragmentActivity?, product: SkuDetails){
        if(activity == null) return
        val flowParams = BillingFlowParams.newBuilder().setSkuDetails(product).build()
        val responseCode = billingClient.launchBillingFlow(activity, flowParams).responseCode
        if (responseCode != BillingClient.BillingResponseCode.OK) {
            onError("Can not start purchase flow, Error code: $responseCode")
        }
    }

    private fun onError(message: String) = Toast.makeText(context, message, Toast.LENGTH_LONG).show()

    private fun String.convertReward(): Int{
        return when(this){
            PRODUCT_300 -> 300
            PRODUCT_4000 -> 4000
            PRODUCT_10000 -> 10000
            PRODUCT_300D -> 600
            PRODUCT_4000D -> 8000
            PRODUCT_10000D -> 20000
            else -> 0
        }
    }

    companion object {
        private const val PRODUCT_TYPE = BillingClient.SkuType.INAPP
        private const val PRODUCT_300 = "300_1"
        private const val PRODUCT_4000 = "4000_5"
        private const val PRODUCT_10000 = "10000_10"
        private const val PRODUCT_300D = "300_1_double"
        private const val PRODUCT_4000D = "4000_1_double"
        private const val PRODUCT_10000D = "10000_1_double"
    }
}