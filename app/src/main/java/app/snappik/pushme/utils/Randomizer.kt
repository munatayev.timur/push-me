package app.snappik.pushme.utils

import app.snappik.pushme.R
import app.snappik.pushme.model.Cell
import app.snappik.pushme.view.custom.KingLayout
import kotlin.random.Random

private const val FILLED_RARITY = 5
private const val EMOTION_RARITY = 95

fun get3RandomItems(): List<Pair<Int, Cell.Store?>>{
    return List(3){ getRandomInventoryItem() }
}

fun getRandomEmotion() : KingLayout.Emotion{
    var d = Math.random() * 100
    return if (EMOTION_RARITY.let { d -= it; d } < 0)
        KingLayout.Emotion(R.id.king_idle, 20000)
    else KingLayout.Emotion(when(randomizeEmotion()){
        1 -> R.id.king_sad
        2 -> R.id.king_funny
        3 -> R.id.king_evil
        4 -> R.id.king_surprise
        5 -> R.id.king_dead
        else -> R.id.king_idle
          }, 5000)
}

private fun getRandomInventoryItem() : Pair<Int, Cell.Store?> {
    var d = Math.random() * 100
    return if (FILLED_RARITY.let { d -= it; d } < 0)
        Pair(randomizeItem(), Cell.Filled())
    else Pair(randomizeMoney(), null)
}

private fun randomizeMoney() = Random.nextInt(10, 50)

private fun randomizeItem() = Random.nextInt(1, 3)

private fun randomizeEmotion() = Random.nextInt(1, 15)