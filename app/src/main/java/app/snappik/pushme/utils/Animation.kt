package app.snappik.pushme.utils

import android.animation.ValueAnimator
import android.util.DisplayMetrics
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter(value = ["endNumber", "duration", "wrapString", "animateWithZero"], requireAll = false)
fun animateChanges(textView: TextView,
                   endNumber: Int = 0,
                   duration: Long = 2000,
                   wrapString: String? = null,
                   animateWithZero: Boolean = false) {
    val init = if(textView.text.isNullOrEmpty()) 0 else textView.text.toString().toInt()
    if(animateWithZero.not() && init == 0) {
        textView.text = if (wrapString != null) String.format(wrapString, endNumber) else endNumber.toString()
        return
    }
    ValueAnimator.ofInt(init, endNumber)
        .setDuration(duration)
        .apply {
            addUpdateListener {
                val value = it.animatedValue.toString()
                textView.text = if (wrapString != null) String.format(wrapString, value) else value
            }
        }
        .start()
}

fun animateCellInit(view:View) {
    ValueAnimator
            .ofInt(0, 100)
            .setDuration(300)
            .apply {
                addUpdateListener {
                    val value = it.animatedValue.toString().toInt()
                    view.alpha = value / 100F
                    view.scaleX = 0.5F + 0.005F * value
                    view.scaleY = 0.5F + 0.005F * value
                }
            }.start()
}

fun RecyclerView.smoothSnapToPosition(position: Int, isReady: Boolean = false) {
    val smoothScroller = object : LinearSmoothScroller(context) {
        override fun getVerticalSnapPreference(): Int = SNAP_TO_ANY
        override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float = 500f / displayMetrics.densityDpi
        override fun onStop() {
            super.onStop()
            if(isReady.not()) smoothSnapToPosition(0, true)
        }
    }
    smoothScroller.targetPosition = position
    layoutManager?.startSmoothScroll(smoothScroller)
}