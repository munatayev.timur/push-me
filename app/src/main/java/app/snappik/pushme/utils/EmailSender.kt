package app.snappik.pushme.utils

import android.content.Context
import android.util.Log
import app.snappik.pushme.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*
import javax.inject.Inject
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

class EmailSender @Inject constructor(
    private val context: Context,
    private val properties: Properties)  {

    private val props = Properties().apply {
        put("mail.smtp.auth", "true")
        put("mail.smtp.starttls.enable", "true")
        put("mail.smtp.host", properties.getProperty(SENDER_EMAIL_HOST))
        put("mail.smtp.port", properties.getProperty(SENDER_EMAIL_PORT))
    }

    private val authCredentials = PasswordAuthentication(properties.getProperty(SENDER_EMAIL), properties.getProperty(SENDER_EMAIL_PASSWORD))
    private val auth = object : Authenticator() { override fun getPasswordAuthentication() = authCredentials }
    private val mFrom = InternetAddress(properties.getProperty(SENDER_EMAIL), context.getString(R.string.app_name))
    private val recipient = InternetAddress.parse(properties.getProperty(SENDER_EMAIL_RECEIVER))

    fun send(author: String, path: String, solution: String) = CoroutineScope(Dispatchers.IO).launch {
        try {
            MimeMessage(Session.getInstance(props, auth))
                .apply {
                    setFrom(mFrom)
                    setRecipients(Message.RecipientType.TO, recipient)
                    subject = author
                    setText("$path\n$solution")
                }.also {
                    Transport.send(it)
                }
        }catch (e: Exception){
            Log.d("TESTTEST", e.message ?: "Error empty")
        }
    }

    companion object {
        private const val SENDER_EMAIL = "pushme_email_login"
        private const val SENDER_EMAIL_PASSWORD = "pushme_email_password"
        private const val SENDER_EMAIL_HOST = "pushme_email_host"
        private const val SENDER_EMAIL_PORT = "pushme_email_port"
        private const val SENDER_EMAIL_RECEIVER = "pushme_email_receiver"
    }
}