package app.snappik.pushme.utils

import app.snappik.pushme.model.Cell
import app.snappik.pushme.model.Chapter
import app.snappik.pushme.model.Guid
import app.snappik.pushme.model.GuidText
import app.snappik.pushme.model.Level
import app.snappik.pushme.model.chapter
import app.snappik.pushme.model.guid
import app.snappik.pushme.model.level

object ParseUtils {

    private const val SPLITTER_MAIN = "|"
    private const val SPLITTER_SOLUTION = ","
    private const val SPLITTER_CHAPTER = "_"

    fun String.parseGuid(): List<Guid> {
        return lines().map {
            val data = it.split(SPLITTER_SOLUTION)
            guid {
                chapterId = data[0].toInt()
                level = data[1].toLong()
                startView = data[2].toInt()
                endView = data[3].toInt()
                text = GuidText.valueOf(data[4])
                adapter = data[5].toInt()
            }
        }
    }

    fun String.parseChapter(): Chapter {
        val data = split(SPLITTER_CHAPTER)
        return chapter {
            order = data[0].toInt()
            name = data[1]
            cost = data[2].toInt()
            topColorHex = data[3]
            bottomColorHex = data[4]
        }
    }

    fun String.parseLevel(cId: Int): List<Level>{
        val lines = lines()
        return lines.mapIndexed { mIndex, s ->
            val data = s.split(SPLITTER_MAIN)
            level {
                chapterId = cId
                index = mIndex + 1
                isDone = false
                path = data[0]
                solution = data[1]
                coins = data[2].toInt()
                isLast = lines.size == mIndex + 1
                lastEntering = if(mIndex == 0) 1L else 0
            }
        }
    }

    fun String.parsePath(): MutableList<Cell>{
        return if(isEmpty()) MutableList(49){ Cell.Void() }
         else toCharArray().map {
            when(it){
                '0' -> Cell.Void()
                '3' -> Cell.Filled()
                '2' -> Cell.Empty()
                '4' -> Cell.Redirect(Cell.Direction.RIGHT)
                '5' -> Cell.Redirect(Cell.Direction.LEFT)
                '6' -> Cell.Redirect(Cell.Direction.TOP)
                '7' -> Cell.Redirect(Cell.Direction.BOTTOM)
                '8' -> Cell.Ice()
                '9' -> Cell.Hole()
                else -> Cell.ActivatorEmpty()
            }
        }.toMutableList()
    }

    fun List<Cell>.parseToPathString():String{
        return if(isEmpty()) ""
        else joinToString(separator = "") {
            when{
                it is Cell.Hole -> "9"
                it is Cell.Ice -> "8"
                it is Cell.Redirect && it.direction == Cell.Direction.RIGHT -> "4"
                it is Cell.Redirect && it.direction == Cell.Direction.LEFT -> "5"
                it is Cell.Redirect && it.direction == Cell.Direction.TOP -> "6"
                it is Cell.Redirect && it.direction == Cell.Direction.BOTTOM -> "7"
                it is Cell.Void -> "0"
                it is Cell.Filled -> "3"
                it is Cell.Empty -> "2"
                else -> "1"
            }
        }
    }

    fun String.parseSolution(): MutableList<Cell.Solution>{
        return if(isEmpty()) mutableListOf()
        else split(SPLITTER_SOLUTION).map {
            when(val type = it.toCharArray()[0]){
                'F' -> Cell.Filled()
                else -> {
                    val counter = it.substring(1, it.length).toInt()
                    val direction = when(type){
                        Cell.Direction.BOTTOM.name.first() -> Cell.Direction.BOTTOM
                        Cell.Direction.LEFT.name.first() -> Cell.Direction.LEFT
                        Cell.Direction.RIGHT.name.first() -> Cell.Direction.RIGHT
                        else -> Cell.Direction.TOP
                    }
                    Cell.Activator(counter, direction)
                }
            }
        }.toMutableList()
    }

    fun MutableList<Cell.Solution>.parseToSolutionString(): String{
        return if(isEmpty()) ""
        else joinToString(separator = SPLITTER_SOLUTION) {
            when(it){
                is Cell.Activator -> {
                    val counter = it.counter
                    val direction = when (it.direction) {
                        Cell.Direction.BOTTOM -> Cell.Direction.BOTTOM.name.first()
                        Cell.Direction.LEFT -> Cell.Direction.LEFT.name.first()
                        Cell.Direction.RIGHT -> Cell.Direction.RIGHT.name.first()
                        else -> Cell.Direction.TOP.name.first()
                    }
                    "$direction$counter"
                }
                else -> "F"
            }
        }
    }

    fun String.parseStore(): MutableList<Cell.Store>{
        return if(length > 0) split(SPLITTER_SOLUTION).map {
            when(it.toCharArray()[0]){
                'F' -> Cell.Filled()
                else -> Cell.Filled()
            }
        }.toMutableList() else mutableListOf()
    }

    fun List<Cell.Store>.parseToStringString(): String {
        return joinToString {
            when (it) {
                is Cell.Filled -> "F"
                else -> "F"
            }
        }
    }
}