package app.snappik.pushme.observer

import app.snappik.pushme.model.Chapter
import app.snappik.pushme.viewmodel.ChapterViewModel

interface IChapterObserver : IAbstractObserver {

    val chapterViewModel: ChapterViewModel

    fun subscribeToChapters() = chapterViewModel.chapters?.subscribe {
        it?.let { onChaptersFetched(it) }
    }

    fun onChaptersFetched(list: List<Chapter>)
}