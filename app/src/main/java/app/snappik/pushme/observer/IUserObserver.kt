package app.snappik.pushme.observer

import androidx.fragment.app.FragmentManager
import app.snappik.pushme.model.User
import app.snappik.pushme.ui.dialog.CoinsDialogFragment
import app.snappik.pushme.viewmodel.UserViewModel

interface IUserObserver : IAbstractObserver {

    val userViewModel: UserViewModel

    fun fetchUser() = userViewModel.user?.observeOnce(true){
        it?.let { onUserFetched(it) } ?: noUser()
    }

    fun subscribeToUserUpdate() = userViewModel.user?.subscribe{
        it?.let { onUserFetched(it) } ?: noUser()
    }

    fun onUserFetched(user: User) = Unit

    fun noUser() = Unit

    fun openCoins(childFragmentManager : FragmentManager) = CoinsDialogFragment().show(childFragmentManager)
}