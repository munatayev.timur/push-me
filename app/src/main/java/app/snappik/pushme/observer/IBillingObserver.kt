package app.snappik.pushme.observer

import app.snappik.pushme.model.BillingProduct
import app.snappik.pushme.utils.BillingManager

interface IBillingObserver : IAbstractObserver {

    val billingManager: BillingManager

    fun fetchProducts() = billingManager.products.observeOnce(true) {
        it?.let { onProductsFetched(it) }
    }

    fun onProductsFetched(list: List<BillingProduct>) = Unit
}