package app.snappik.pushme.observer

import app.snappik.pushme.model.Level
import app.snappik.pushme.viewmodel.LevelViewModel

interface ILevelObserver : IAbstractObserver {

    val levelViewModel: LevelViewModel

    fun fetchLevel(levelId: Long, isDone: Boolean = false) = levelViewModel.getLevelById(levelId)?.observeOnce {
        it?.let { if(isDone.not()) onLevelFetched(it) else showDoneDialog(it) }
    }

    fun fetchLevels(chapterId: Long) = levelViewModel.getLevelsByChapterId(chapterId)?.subscribe { it?.let { onLevelsFetched(it) } }

    fun fetchLastActive(isDone: Boolean = false) = levelViewModel.getLastActiveLevel()?.observeOnce {
        it?.let { if(isDone.not()) onLevelFetched(it) else showDoneDialog(it) }
    }

    fun subscribeToAdds() = levelViewModel.addCounter.subscribe {
        it?.let { if(it == 0) onShowAdd() }
    }

    fun showDoneDialog(level: Level) = Unit

    fun onLevelFetched(level: Level) = Unit

    fun onLevelsFetched(list: List<Level>) = Unit

    fun onShowAdd() = Unit
}