package app.snappik.pushme.observer

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

interface IAbstractObserver : LifecycleOwner {

    fun <T> LiveData<T>.unsubscribe() {
        this.removeObservers(this@IAbstractObserver)
    }

    fun <T> LiveData<T>.subscribe(postman: ((data: T?) -> Unit)? = null) {
        observe(this@IAbstractObserver, {
            postman?.invoke(it)
        })
    }

    fun <T> LiveData<T>.observeOnce(allowSameData: Boolean = false, postman: ((data: T?) -> Unit)? = null) {
        val initialValue = if(allowSameData) null else value
        observe(this@IAbstractObserver, object : Observer<T>{
            override fun onChanged(t: T) {
                if(initialValue == t) return
                postman?.invoke(t)
                removeObserver(this)
            }
        })
    }
}