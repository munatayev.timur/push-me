package app.snappik.pushme.observer

import app.snappik.pushme.model.Guid
import app.snappik.pushme.viewmodel.GuidViewModel

interface IGuidObserver : IAbstractObserver {

    val guidViewModel: GuidViewModel

    fun getGuidForLevel(levelId: Int, chapterId: Int) = guidViewModel.getGuidByLevel(levelId, chapterId)?.subscribe {
        it?.let { onGuidFetched(it) } ?: noGuid()
    }

    fun onGuidFetched(guid: Guid) = Unit

    fun noGuid() = Unit
}